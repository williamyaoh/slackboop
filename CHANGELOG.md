# Changes

## Unreleased

* Fixed server to do a whole bunch of things:
  * Check for duplicate events and ignore them
  * Allow logging errors when endpoints are missed/bad things happen
  * Do background processing instead of forcing everything to be synchronous
