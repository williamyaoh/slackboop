#!/usr/bin/env python3

## When setting up your Lambda function, make sure to set the runtime
## to Python 3.6 and the handler name to `api-gateway-handler.handle'.

import sys
import subprocess
import json

def handle(event, context):
    output = subprocess.run(
        ["./lambda-handler"],
        encoding = "utf8",
        stdout = subprocess.PIPE,
        input = json.dumps(event)
    )

    payload = json.loads(output.stdout)

    return payload
