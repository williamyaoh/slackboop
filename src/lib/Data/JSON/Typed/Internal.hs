{-# OPTIONS_GHC -Wno-redundant-constraints #-}
{-# OPTIONS_GHC -Wno-unticked-promoted-constructors #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeApplications #-}

module Data.JSON.Typed.Internal where

import qualified Prelude ()
import Prologue

import Data.Proxy
import Data.Aeson
import Data.Aeson.Types
import Data.HashMap.Strict
import Data.Kind ( Constraint )

import GHC.TypeLits
import qualified GHC.TypeLits as Y
import GHC.Exts ( Any )

import Unsafe.Coerce ( unsafeCoerce )

data JPropType = Required | Optional

data JProp (pty :: JPropType) (pname :: Symbol) (ty :: *) where
  JRequired :: a -> JProp 'Required pname a
  JOptional :: Maybe a -> JProp 'Optional pname a

-- |
-- It's useful to define our JSON types such that we can accept anything
-- containing just the properties we care about, while keeping around the
-- extra fields that we don't care about.
--
-- For example, given the following JSON input:
--
-- @{ "foo": 42, "bar": "hello" }@
--
-- we /could/ define a type @Baz@:
--
-- > type Baz = J '[ "foo" .! Int ]
--
-- But if we parsed the input as a @Baz@ and then wrote it back out as JSON,
-- we would lose the @"bar"@ property. If that's not what we want, but we also
-- don't particularly care what the other values in the object are, we can
-- use @ExtraProps@ to define our type:
--
-- > type Baz = J '[ "foo" .! Int, ExtraProps ]
--
-- and now we'll retain even the fields we haven't mentioned.
data ExtraProps

type (.!) = JProp 'Required
type (.?) = JProp 'Optional

jreq :: forall label a. ToJSON a => a -> JProp 'Required label a
jreq a = JRequired a

jopt :: forall label a. ToJSON a => Maybe a -> JProp 'Optional label a
jopt ma = JOptional ma

newtype J (props :: [*]) =
  -- The HashMap contains all the fields specified in @props@, while the
  -- Object contains ExtraProps, if that's been specified.
  J (HashMap Text Any, Object)

-- | This typeclass is just a mirror of `FromJSON', which is here to provide
--   a level of indirection so that we can hang a property name uniqueness
--   constraint on @FromJSON@ without needing to specify it multiple times.
class FromJSON' c where
  parseJSON' :: Value -> Parser c

instance FromJSON' (J '[]) where
  parseJSON' = withObject "J" $ \_ ->
    pure $ J (empty, empty)

instance ( KnownSymbol sym
         , FromJSON' (J props)
         , FromJSON a
         ) => FromJSON' (J (JProp 'Required sym a ': props)) where
  parseJSON' val = flip (withObject "J") val $ \obj -> do
    J (map, extra) <- parseJSON' @(J props) val
    val <- (.:) @a obj propName
    pure $ J (insert propName (unsafeCoerce val) map, extra)
    where propName :: Text
          propName = pack $ symbolVal (Proxy @sym)

instance ( KnownSymbol sym
         , FromJSON' (J props)
         , FromJSON a
         ) => FromJSON' (J (JProp 'Optional sym a ': props)) where
  parseJSON' val = flip (withObject "J") val $ \obj -> do
    J (map, extra) <- parseJSON' @(J props) val
    val <- (.:?) @a obj propName
    pure $ case val of
      Nothing -> J (map, extra)
      Just val -> J (insert propName (unsafeCoerce val) map, extra)
    where propName :: Text
          propName = pack $ symbolVal (Proxy @sym)

instance FromJSON' (J props) => FromJSON' (J (ExtraProps ': props)) where
  parseJSON' val = flip (withObject "J") val $ \obj -> do
    J (map, extra) <- parseJSON' @(J props) val
    pure $ J (map, obj <> extra)

instance ( FromJSON' (J props)
         , AreUniqueProps props
         ) => FromJSON (J props) where
  parseJSON = parseJSON'
  {-# INLINE parseJSON #-}

class ToJSONAccum c where
  toJSONAccum :: c -> HashMap Text Value

instance ToJSONAccum (J '[]) where
  toJSONAccum (J (_, extra)) = extra

instance ( KnownSymbol sym
         , ToJSONAccum (J props)
         , ToJSON a
         ) => ToJSONAccum (J (JProp 'Required sym a ': props)) where
  toJSONAccum (J (map, extra)) =
    let val = toJSON $ unsafeCoerce @_ @a (map ! propName)
    in insert propName val (toJSONAccum @(J props) (J (map, extra)))
    where propName :: Text
          propName = pack $ symbolVal (Proxy @sym)

instance ( KnownSymbol sym
         , ToJSONAccum (J props)
         , ToJSON a
         ) => ToJSONAccum (J (JProp 'Optional sym a ': props)) where
  toJSONAccum (J (map, extra)) =
    let submap = toJSONAccum @(J props) (J (map, extra))
    in case lookup propName map of
         Nothing -> submap
         Just any -> insert propName (toJSON $ unsafeCoerce @_ @a any) submap
    where propName :: Text
          propName = pack $ symbolVal (Proxy @sym)

instance ToJSONAccum (J props) => ToJSONAccum (J (ExtraProps ': props)) where
  toJSONAccum (J (map, extra)) =
    extra <> toJSONAccum @(J props) (J (map, extra))

instance ( ToJSONAccum (J props)
         , AreUniqueProps props
         ) => ToJSON (J props) where
  toJSON j = toJSON $ toJSONAccum j

instance ToJSON (J props) => Show (J props) where
  show = show . toJSON

-- | Find the property in @props@ with the given name and transform its type.
--   We assume that there's only a single instance of the given property.
type family ReplaceType (sym :: Symbol) a a' (props :: [*]) :: [*] where
  ReplaceType _ _ _ '[] = '[]
  ReplaceType sym a a' ((sym .! a) ': props) = (sym .! a') ': props
  ReplaceType sym a a' (prop ': props) = prop ': ReplaceType sym a a' props

-- |
-- `Has', but specialized on a `J' type.
type family JHas a props where
  JHas (J actual) props = Has props actual
  JHas _ _ = TypeError ( 'Y.Text "not a JSON type" )

-- |
-- Your bread-and-butter constraint for reading in typed JSON. The idea is
-- that instead of tying your algorithm to a concrete JSON structure, all
-- you actually need is for the structure to have certain fields containing
-- specific types, and whether the structure has other fields is irrelevant.
-- So instead, you can specify what you need using @Has@, and get a more
-- reusable function.
--
-- > showField :: ( Show a, Has '[ "foo" .! a ] props ) => J props -> IO ()
-- > showField = putStrLn . show . getreq @"foo"
type family Has (reqprops :: [*]) (props :: [*]) :: Constraint where
  Has reqprops props =
    ( AreUniqueProps props
    , HasAllPropNames reqprops props
    , HasRequiredStrictness reqprops props
    , HasTypeAssociations reqprops props
    )

type family JPropNames (props :: [*]) :: [Symbol] where
  JPropNames '[] = '[]
  JPropNames (JProp _ name _ ': props) = name ': JPropNames props
  JPropNames (ExtraProps ': props) = JPropNames props

type family If (cond :: Bool) (t :: a) (f :: a) :: a where
  If 'True t f = t
  If 'False t f = f

type family In (sym :: a) (syms :: [a]) :: Bool where
  In sym '[] = 'False
  In sym (sym ': syms) = 'True
  In sym (sym' ': syms) = In sym syms

type family Duplicates' (xs :: [a]) (seen :: [a]) (dup :: [a]) :: [a] where
  Duplicates' '[] seen dup = dup
  Duplicates' (x ': xs) seen dup =
    If (In x seen)
       (If (In x dup)
           (Duplicates' xs seen dup)
           (Duplicates' xs seen (x ': dup)))
       (Duplicates' xs (x ': seen) dup)

type family Duplicates (xs :: [a]) :: [a] where
  Duplicates xs = Duplicates' xs '[] '[]

type family AreUniqueProps' (propsE :: [*]) (dups :: [Symbol]) :: Constraint where
  AreUniqueProps' propsE '[] = ()
  AreUniqueProps' propsE dups = TypeError
       ( Y.Text "Hey, the property(s) " :<>: ShowType dups
    :$$: Y.Text "each appear multiple times in this JSON type:"
    :$$: ShowType (J propsE)
    :$$: Y.Text ""
    :$$: Y.Text "JSON objects cannot directly contain more than one of the same property name."
    :$$: Y.Text ""
       )

type family AreUniqueProps (props :: [*]) :: Constraint where
  AreUniqueProps props = AreUniqueProps' props (Duplicates (JPropNames props))

type family HasPropName (sym :: Symbol) (props :: [*]) (yes :: Bool) :: Constraint where
  HasPropName sym props 'True = ()
  HasPropName sym props 'False = TypeError
       ( Y.Text "Hey, I can't find any property named " :<>: ShowType sym
    :$$: Y.Text "in this JSON type:"
    :$$: ShowType (J props)
    :$$: Y.Text ""
       )

type family HasAllPropNames (toFind :: [*]) (props :: [*]) :: Constraint where
  HasAllPropNames '[] props = ()
  HasAllPropNames (JProp ty sym a ': fs) props =
    ( HasPropName sym props (In sym (JPropNames props))
    , HasAllPropNames fs props
    )

type family PropHasStrictness (prop :: *) (props :: [*]) :: Bool where
  PropHasStrictness (JProp 'Required sym a) (JProp 'Required sym a' ': props) = 'True
  PropHasStrictness (JProp 'Required sym a) (JProp 'Optional sym a' ': props) = 'False
  PropHasStrictness (JProp 'Optional sym a) (JProp 'Required sym a' ': props) = 'True
  PropHasStrictness (JProp 'Optional sym a) (JProp 'Optional sym a' ': props) = 'True
  PropHasStrictness prop (prop' ': props) = PropHasStrictness prop props

type family PropHasStrictnessE (propE :: *) (propsE :: [*]) (yes :: Bool) :: Constraint where
  PropHasStrictnessE propE propsE 'True = ()
  PropHasStrictnessE propE propsE 'False = TypeError
       ( Y.Text "Hey, I'm looking for a property " :<>: Y.Text "(" :<>: ShowType propE :<>: Y.Text ")"
    :$$: Y.Text "in this JSON type:"
    :$$: ShowType (J propsE)
    :$$: Y.Text "but the JSON has a weaker requiredness on the property than"
    :$$: Y.Text "I'm looking for. I need the property to be 'Required, but it"
    :$$: Y.Text "seems to be 'Optional in this JSON type."
    :$$: Y.Text ""
       )

type family HasRequiredStrictness (reqprops :: [*]) (props :: [*]) :: Constraint where
  HasRequiredStrictness '[] props = ()
  HasRequiredStrictness (JProp ty sym a ': rs) props =
    ( PropHasStrictnessE (JProp ty sym a) props (PropHasStrictness (JProp ty sym a) props )
    , HasRequiredStrictness rs props
    )

type family CorrespondingType (sym :: Symbol) (props :: [*]) :: * where
  CorrespondingType _ '[] = TypeError (Y.Text "couldn't find corr")
  CorrespondingType sym (JProp _ sym a ': _) = a
  CorrespondingType sym (_ ': props) = CorrespondingType sym props

type family HasCompatibleType (prop :: *) (props :: [*]) (propsE :: [*]) :: Constraint where
  HasCompatibleType (JProp jty sym a) props propsE = a ~ CorrespondingType sym props

-- |
-- Check that all the properties that we're looking for have a compatible type
-- within the actual properties of the JSON object.
type family HasTypeAssociations (reqprops :: [*]) (props :: [*]) :: Constraint where
  HasTypeAssociations '[] props = ()
  HasTypeAssociations (r ': rs) props =
    ( HasCompatibleType r props props, HasTypeAssociations rs props )

type family AddProp (props :: [*]) (prop :: *) :: [*] where
  AddProp '[] prop = '[prop]
  AddProp (JProp _ label _ ': ps) (JProp pty label a) = JProp pty label a ': ps
  AddProp (p ': ps) prop = p ': AddProp ps prop
