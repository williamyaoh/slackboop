{-# OPTIONS_GHC -Wno-redundant-constraints #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

-- |
-- Module      : Data.JSON.Typed
-- Description : Accessing fields in JSON in a type-safe way.
-- Copyright   : (c) 2018 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : GHC
--
-- JSON may be described in some circles as a flexible format that can take any
-- shape you want. /Usually/, though, you /don't/ want your interchange format
-- to take any shape. It makes it kind of hard to work with.
--
-- This module provides a way of very easily describing what you want the
-- structure of your JSON to look like, while also providing a way to write
-- generic computations that work on any JSON structure with the requisite
-- fields.

module Data.JSON.Typed
  (
  -- * Basic usage
  -- $usage
    J, JProp, JPropType(..), ExtraProps, type (.!), type (.?)
  , jempty, jadd, (|~)
  , JHas, Has
  -- * Basic getters
  , getreq, getopt
  -- * Lenses
  , _j, _jo
  )
where

import qualified Prelude ()
import Prologue

import Data.Proxy
import Data.HashMap.Strict

import GHC.TypeLits

import Unsafe.Coerce ( unsafeCoerce )

import Data.JSON.Typed.Internal

-- $usage
--
-- Here's an example of a JSON type we might define:
--
-- @
-- {-\# LANGUAGE DataKinds \#-}
--
-- import Data.JSON.Typed
--
-- type User =
--   'J' '[ 'JProp' ''Required' "userid" 'Int'
--      , 'JProp' ''Required' "username" 'Text'
--      , 'JProp' ''Optional' "email" 'Text'
--      , 'JProp' ''Optional' "description" 'Text'
--      ]
-- @
--
-- And with this, we automatically have a way to parse instances of our type!
--
-- @
-- {-\# LANGUAGE TypeApplications \#-}
--
-- 'eitherDecode' @User someInput
-- @
--
-- If we want to use our newly-parsed data, instead of tying our function
-- directly to our @User@ type, we can just describe which fields we
-- need and which types they have:
--
-- @
-- {-\# LANGUAGE DataKinds \#-}
-- {-\# LANGUAGE TypeApplications \#-}
-- {-\# LANGUAGE OverloadedStrings \#-}
--
-- maintainerString :: Has '[ JProp 'Required "username" Text, JProp 'Optional "email" Text ] props
--                  => J props
--                  -> Text
-- maintainerString j =
--   let username = 'getreq' \@\"username\" j
--       email = 'getopt' \@\"email\" j
--   in case email of
--        Nothing -> username
--        Just e -> 'mconcat' [ username, \" \", \"\<\", e, \"\>\" ]
-- @
--
-- Now, we can use @maintainerString@ on any JSON structure that contains the
-- fields that we want! Useful if we have many different bits of JSON with
-- some shared structure.
--
-- Finally, we can use @"\<name\>" .! a@ and @"\<name\>" .? a@ as synonyms for
-- @JProp 'Required "\<name\>" a@ and @JProp 'Optional "\<name\>" a@, respectively,
-- for conciseness. So we can rewrite our earlier types as:
--
-- @
-- type User = J '[ "userid" '.!' Int
--                , "username" '.!' Text
--                , "email" '.?' Text
--                , "description" '.?' Text
--                ]
--
-- maintainerString :: Has '[ "username" '.!' Text, "email" '.?' Text ] props
--                  => J props
--                  -> Text
-- @

getreq :: forall sym props a.
          ( KnownSymbol sym
          , Has '[ sym .! a ] props
          )
       => J props
       -> a
getreq (J (map, _)) =
  let key = pack $ symbolVal (Proxy @sym)
  in unsafeCoerce (map ! key)

getopt :: forall sym props a.
          ( KnownSymbol sym
          , Has '[ sym .? a ] props
          )
       => J props
       -> Maybe a
getopt (J (map, _)) =
  let key = pack $ symbolVal (Proxy @sym)
  in unsafeCoerce (lookup key map)

-- |
-- The empty `J'. Unfortunately, we can't really define a `Monoid' instance
-- because the type would change on `mappend's.
jempty :: J '[]
jempty = J (empty, empty)

jadd :: forall a pty key props1 props2 prop.
       (prop ~ JProp pty key a, props2 ~ AddProp props1 prop, KnownSymbol key)
     => J props1
     -> prop
     -> J props2
jadd (J (known, extra)) prop =
  let key = pack $ symbolVal (Proxy :: Proxy key)
  in case prop of
    JRequired a -> J (insert key (unsafeCoerce a) known, extra)
    JOptional a -> case a of
      Nothing -> J (known, extra)
      Just a -> J (insert key (unsafeCoerce a) known, extra)

infixl 7 |~

(|~) :: forall a pty key props1 props2 prop.
       (prop ~ JProp pty key a, props2 ~ AddProp props1 prop, KnownSymbol key)
     => J props1
     -> prop
     -> J props2
(|~) = jadd

_j :: forall sym a a' f props1 props2.
      ( KnownSymbol sym
      , Functor f
      , Has '[ sym .! a ] props1
      , props2 ~ ReplaceType sym a a' props1
      )
   => (a -> f a')
   -> (J props1 -> f (J props2))
_j f (J (map, extra)) =
  let x = f $ unsafeCoerce (map ! key)
  in (flip fmap) x $ \inner ->
       J (insert key (unsafeCoerce inner) map, extra)
  where key :: Text
        key = pack $ symbolVal (Proxy @sym)

_jo :: forall sym a a' f props1 props2.
       ( KnownSymbol sym
       , Functor f
       , Has '[ sym .? a ] props1
       , props2 ~ ReplaceType sym a a' props1
       )
    => (Maybe a -> f (Maybe a'))
    -> (J props1 -> f (J props2))
_jo f (J (map, extra)) =
  let x = f $ unsafeCoerce $ lookup key map
  in (flip fmap) x $ \case
    Nothing -> J (map, extra)
    Just value -> J (insert key (unsafeCoerce value) map, extra)
  where key :: Text
        key = pack $ symbolVal (Proxy @sym)
