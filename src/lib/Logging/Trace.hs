{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}

{-|
Module      : Logging.Trace
Description : Extra interpreters for `freer-simple's Trace effect.
Copyright   : (c) William Yao, 2018
License     : BSD-3
Maintainer  : williamyaoh@gmail.com
Stability   : experimental
Portability : POSIX

Admittedly, `Trace' is a woefully underpowered effect, with not a whole lot of
customization available. Still, it's useful as a way to provide a logging
interface without a lot of fuss for prototype implementations or beginning
forays into an effect algebra.

So we provide some useful interpreters for `Trace', so that it can serve its
purpose a little better.
-}

module Logging.Trace
  ( runTrace, runTraceCollect, runTraceBlackHole )
where

import qualified Prelude ( String )
import Prologue

import System.IO

import Control.Monad.Freer
import Control.Monad.Freer.Trace hiding ( runTrace )
import Control.Monad.Freer.Writer

-- | Run the trace, logging to `stderr' and leaving it in an Eff. Not sure
--   why `freer-simple' doesn't already provide this.
runTrace :: LastMember IO effs => Eff (Trace ': effs) ~> Eff effs
runTrace = interpretM $ \case
  Trace str -> hPutStrLn stderr str

-- | Run the trace, and collect all the traces in-memory, outputting
--   a list of traced strings.
runTraceCollect :: Eff (Trace ': effs) a -> Eff effs (a, [Prelude.String])
runTraceCollect = runWriter . reinterpret algebra
  where algebra :: Trace ~> Eff (Writer [Prelude.String] ': effs)
        algebra = \case
          Trace str -> tell [str]

-- | Run the trace, and discard all output.
runTraceBlackHole :: Eff (Trace ': effs) ~> Eff effs
runTraceBlackHole = interpret $ \case
  Trace _ -> pure ()
