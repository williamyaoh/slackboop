{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}

module Chat.Slackboop where

import qualified Prelude ()
import Prologue hiding ( fail )

import Data.Aeson
import Data.String.Interpolate ( i )
import qualified Data.ByteString.Lazy as BL

import Control.Monad
import Control.Monad.Freer
import Control.Monad.Freer.Trace hiding ( runTrace )

import GHC.Generics

import qualified Servant as S
import qualified Network.Wai.Handler.Warp as Warp

import System.Environment

import Network.HTTP.Client
import Network.HTTP.Client.TLS

import Logging.Trace

import qualified Chat.Slackboop.API as API

import Chat.Slackboop.SlashCommands.Types

import Chat.Slackboop.Backends.Types
import Chat.Slackboop.Backends.Localhost
import Chat.Slackboop.Backends.APIGatewayStdio

-- | A simpler type definition so that you don't have to deal with freer monads
--   and effect algebras for simpler cases.
type SlackAPI =
  Eff '[API.SlackAPI, Trace, IO]

type Handler m i o = (i -> m Bool, i -> m o)

type EventHandler' m = SlackboopSettings -> Handler m Value (Maybe Value)
type EventHandler = EventHandler' SlackAPI

type InteractionHandler' m = SlackboopSettings -> Handler m Value ()
type InteractionHandler = InteractionHandler' SlackAPI

type SlashCmdHandler' m = SlackboopSettings -> Handler m SlashCmdData ()
type SlashCmdHandler = SlashCmdHandler' SlackAPI

data Slackboop' (m :: * -> *) =
  Slackboop
    { eventHandlers :: [EventHandler' m]
    , interactionHandlers :: [InteractionHandler' m]
    , slashCmdHandlers :: [SlashCmdHandler' m]
    }

type Slackboop =
  Slackboop' SlackAPI

-- | Slack and your app have some shared secrets that your app uses to verify
--   that the request is actually coming from Slack, and not being spoofed
--   by a third party. We bundle them together and set them upon starting
--   up the Slack app.
data SlackboopSettings =
  SlackboopSettings
    { sbSigningSecret :: Text
    , sbAppID :: Text
    }
  deriving (Eq, Show, Generic)

-- | When you initially set your app up and subscribe to events coming from
--   Slack, Slack will send a POST request to the subscribed URL in order to
--   verify that you actually control it.
data URLVerification =
  URLVerification
    { verificationChallenge :: Text }
  deriving (Eq, Show, Generic)

data SlackEvent =
  SlackEvent
    { eventID :: Text
    , eventTeamID :: Text
    , eventAppID :: Text
    , eventTime :: Int
    , eventData :: Value
    }
  deriving (Eq, Show, Generic)

instance FromJSON URLVerification where
  parseJSON = withObject "URLVerification" $ \obj -> do
    typeToken :: Text <- obj .: "type"
    if | typeToken /= "url_verification" ->
         fail "didn't get `url_verification' type"
       | otherwise -> URLVerification
         <$> obj .: "challenge"

instance FromJSON SlackEvent where
  parseJSON = withObject "SlackEvent" $ \obj -> do
    typeToken :: Text <- obj .: "type"
    if | typeToken /= "event_callback" ->
         fail "didn't get `event_callback' type"
       | otherwise -> SlackEvent
         <$> obj .: "event_id"
         <*> obj .: "team_id"
         <*> obj .: "api_app_id"
         <*> obj .: "event_time"
         <*> obj .: "event"

verificationHandler :: forall m. Monad m => EventHandler' m
verificationHandler _settings =
  (checkIsVerification, echoVerification)

  where checkIsVerification :: Value -> m Bool
        checkIsVerification value = pure $
          case fromJSON @URLVerification value of
            Error _ -> False
            Success _ -> True

        echoVerification :: Value -> m (Maybe Value)
        echoVerification value = do
          let urlv = fromJSON @URLVerification value
          case urlv of
            Error _ -> undefined  -- impossible
            Success urlv ->
              pure $ Just $ object [ "challenge" .= verificationChallenge urlv ]

-- | Create a handler for Slack Event API events by only needing to match the
--   inner, wrapped event. We'll probably want to clean this interface up a bit...
eventHandler :: forall m event. (FromJSON event, Monad m)
             => (event -> m ())
             -> EventHandler' m
eventHandler innerHandler settings =
  (checkWhole, handleWhole)

  where checkWhole :: Value -> m Bool
        checkWhole value = pure $ case parseWhole value of
          Error _ -> False
          Success _ -> True

        handleWhole :: Value -> m (Maybe Value)
        handleWhole value = do
          case fromJSON @SlackEvent value of
            Error _ -> pure Nothing
            Success envelope ->
              if | sbAppID settings /= eventAppID envelope ->
                   pure Nothing
                 | otherwise ->
                   case fromJSON @event (eventData envelope) of
                     Error _ -> pure Nothing
                     Success event -> do
                       innerHandler event
                       pure Nothing

        parseWhole :: Value -> Result event
        parseWhole value = do
          envelope <- fromJSON @SlackEvent value
          fromJSON @event (eventData envelope)

-- Because of the flow-based way in which it's possible to respond to Slack
-- API responses, we'll need to revise this later...
slackAPIRequest :: Text -> Text -> Value -> IO (Response BL.ByteString)
slackAPIRequest accessToken api value = do
  let apiEndpoint = formatToString ("https://slack.com/api/" % t') api

  manager <- newManager tlsManagerSettings
  baseRequest <- parseRequest apiEndpoint

  let body = encode value
  let request = baseRequest
                  { method = "POST"
                  , requestHeaders =
                    [ ("Authorization", [i|Bearer #{accessToken}|])
                    , ("Content-Type", "application/json;charset=utf-8")
                    , ("Content-Length", [i|#{BL.length body}|])
                    ]
                  , requestBody = RequestBodyLBS body
                  }

  httpLbs request manager

-- | Find the handler that should be run for the given input, if there is one.
findHandler :: Monad m => [Handler m i o] -> i -> m (Maybe (Handler m i o))
findHandler handlers input = do
  filtered <- filterM checkHandler handlers
  case filtered of
    [] -> pure Nothing
    (h:_) -> pure $ Just h

  where checkHandler handler =
          let (pred, _) = handler
          in pred input

-- | Given a list of handlers and some input, run the handler we're supposed
--   to run for the given input.
handleInput :: (Monad m, Default o) => [Handler m i o] -> i -> m o
handleInput handlers input = do
  h <- findHandler handlers input
  case h of
    Nothing -> pure def
    Just handler ->
      let (_, body) = handler
      in body input

-- | Given a specification for a Slackboop app, turn the handlers into
--   the finalized handler functions we need for running the app itself.
compileSlackHandlers :: forall m. Monad m
                     => (m ~> IO)
                     -> SlackboopSettings
                     -> Slackboop' m
                     -> SlackHandlers
compileSlackHandlers transform settings slackboop =
  let Slackboop { .. } = slackboop
  in SlackHandlers
       { onEvent = compile $ (verificationHandler : eventHandlers)
       , onItxn = compile interactionHandlers
       , onSlashCmd = compile slashCmdHandlers
       }
  where compile :: Default o => [SlackboopSettings -> Handler m i o] -> i -> IO o
        compile handlers = fmap transform $ handleInput $ fmap ($ settings) handlers

-- | In a real production setting, it's better to provide secrets (like the
--   signing secret for your Slack app) through environment variables than
--   through hard-coding values. Here, we pull our `SlackboopSettings' from
--   the environment variables `SLACK_APP_ID' and `SLACK_SIGNING_SECRET'.
--
--   NOTE: If you're running an app in production and absolutely need it
--   locked down, don't even use this; use a dedicated secrets manager instead.
envSettings :: IO SlackboopSettings
envSettings = SlackboopSettings
  <$> (pack <$> getEnv "SLACK_SIGNING_SECRET")
  <*> (pack <$> getEnv "SLACK_APP_ID")

-- | Start the Slack bot as a web server listening on localhost, pulling
--   the Slackboop settings and access token from environment variables.
--   Useful for testing your bot by running on your dev machine and tunneling
--   through `ngrok'.
runSlackboopLocal :: Warp.Port -> Slackboop -> IO ()
runSlackboopLocal port slackboop = do
  settings <- envSettings
  hoistSlackboopLocal (runM . runTrace . API.runSlackAPIEnv) port slackboop settings

-- | Start the Slack bot as a web server listening on localhost,
--   except that you can provide a custom interpreter for your handler
--   monad of choice. If you need more complicated context (say, randomness
--   or shared state), use this.
hoistSlackboopLocal :: Monad m
                    => (m ~> IO)
                    -> Warp.Port
                    -> Slackboop' m
                    -> SlackboopSettings
                    -> IO ()
hoistSlackboopLocal transform port slackboop settings =
 Warp.run port slackboopServer
 where SlackHandlers { .. } = compileSlackHandlers transform settings slackboop
       slackboopServer =
         let server = mkSlackboopLocalServer (sbSigningSecret settings) onEvent onItxn onSlashCmd
         in S.serve slackboopLocalProxy server

-- | Start the Slack bot listening for a single event on stdin, writing back
--   a single value on stdout. This is mostly useful for integration with
--   AWS Lambda. Pulls settings from environment variables.
runSlackboopStdio :: Slackboop -> IO ()
runSlackboopStdio slackboop = do
  settings <- envSettings
  hoistSlackboopStdio runSlackboop slackboop settings
  where runSlackboop :: Eff '[API.SlackAPI, Trace, IO] ~> IO
        runSlackboop = runM . runTrace . API.runSlackAPIEnv

-- | Start the Slack bot listening for a single event on stdin, writing back
--   a single value on stdout. This is mostly useful for integration with
--   AWS Lambda.
hoistSlackboopStdio :: Monad m
                    => (m ~> IO)
                    -> Slackboop' m
                    -> SlackboopSettings
                    -> IO ()
hoistSlackboopStdio transform slackboop settings =
  mkSlackboopAPIGatewayStdio (sbSigningSecret settings) slackHandlers
  where slackHandlers = compileSlackHandlers transform settings slackboop
