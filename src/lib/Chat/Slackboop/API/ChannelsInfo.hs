{-|
Module      : Chat.Slackboop.API.ChannelsInfo
Description : Detailed information about Slack channels.

This module is meant to be imported qualified.
-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Chat.Slackboop.API.ChannelsInfo
  ( Channels_Info
  , ChannelInfoRequest(..), ChannelInfoResponse(..)
  )
where

import qualified Prelude ()
import Prologue

import Data.Aeson
import Web.FormUrlEncoded

import Chat.Slackboop.API
import Chat.Slackboop.API.Types

-- | Look up detailed information for a specific channel.
data Channels_Info

type ChannelID = Text
type UserID = Text

data ChannelInfoRequest =
  ChannelInfoRequest
    { channel :: ChannelID }
  deriving (Eq, Show)

data ChannelInfoResponse =
  ChannelInfoResponse
    { respChannelID :: ChannelID
    , respChannelName :: Text
    , respChannelNameNormalized :: Text
    , respChannelPreviousNames :: [Text]
    , respIsChannel :: Bool
    , respCreated :: Int
    , respCreator :: UserID
    , respIsArchived :: Bool
    , respIsGeneral :: Bool
    , respIsShared :: Bool
    , respIsOrgShared :: Bool
    , respIsMember :: Bool
    , respIsPrivate :: Bool
    , respIsMPIM :: Bool
    , respMembers :: [UserID]
    -- TODO: missing: `last_read', `latest', `unread_count', `unread_count_display',
    --                `topic', `purpose'
    }
  deriving (Eq, Show)

instance ToForm ChannelInfoRequest where
  toForm (ChannelInfoRequest {..}) =
    toForm [ "channel" &= channel ]

instance FromJSON ChannelInfoResponse where
  parseJSON = withObject "ChannelInfoResponse" $ \envelope -> do
    obj <- envelope .: "channel"
    ChannelInfoResponse
      <$> obj .: "id"
      <*> obj .: "name"
      <*> obj .: "name_normalized"
      <*> obj .: "previous_names"
      <*> obj .: "is_channel"
      <*> obj .: "created"
      <*> obj .: "creator"
      <*> obj .: "is_archived"
      <*> obj .: "is_general"
      <*> obj .: "is_shared"
      <*> obj .: "is_org_shared"
      <*> obj .: "is_member"
      <*> obj .: "is_private"
      <*> obj .: "is_mpim"
      <*> obj .: "members"

instance WebAPIEndpoint Channels_Info where
  type InputType Channels_Info = ChannelInfoRequest
  type InputContentType Channels_Info = URLFormEncoded
  type OutputType Channels_Info = ChannelInfoResponse
  type OutputContentType Channels_Info = JSON

  endpoint _ = "channels.info"
