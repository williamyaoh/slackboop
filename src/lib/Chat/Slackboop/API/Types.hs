module Chat.Slackboop.API.Types
  ( (&=)
  )
where

import qualified Prelude ()
import Prologue

import Web.Internal.HttpApiData

-- | Tupling + form data conversion. Convenience function for constructing
--   @x-www-form-urlencoded@ data, as it has better type inference.
--
--   The name alludes to the structure of query parameters: @/api?foo=<foo>&bar=<bar>@
--
-- > instance ToForm SomeType where
-- >   toForm someType =
-- >     toForm [ "foo" &= foo someType
-- >            , "bar" &= bar someType
-- >            ]
(&=) :: ToHttpApiData v => Text -> v -> (Text, Text)
(&=) k v = (k, toQueryParam v)
