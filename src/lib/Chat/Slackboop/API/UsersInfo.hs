{-|
Module      : Chat.Slackboop.API.UsersInfo
Description : Detailed information about Slack users.

This module is meant to be imported qualified.
-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Chat.Slackboop.API.UsersInfo
  ( Users_Info
  , UserInfoRequest(..), UserInfoResponse(..)
  )
where

import qualified Prelude ()
import Prologue

import Data.Aeson
import Web.FormUrlEncoded

import Chat.Slackboop.API
import Chat.Slackboop.API.Types ( (&=) )

-- | Look up detailed information for a specific user. Works for
--   bot users, too.
data Users_Info

type UserID = Text
type TeamID = Text
type Color = Text
type Timezone = Text

data UserInfoRequest =
  UserInfoRequest
    { user :: UserID }
  deriving (Eq, Show)

data UserInfoResponse =
  UserInfoResponse
    { respUserID :: UserID
    , respTeamID :: TeamID
    , respUserName :: Text
    , respDeleted :: Bool
    , respColor :: Color
    , respUserRealName :: Text
    , respTimezone :: Timezone
    , respTimezoneLabel :: Text
    , respTimezoneOffset :: Int
    -- TODO: missing: `profile'
    , respIsAdmin :: Bool
    , respIsOwner :: Bool
    , respIsPrimaryOwner :: Bool
    , respIsRestricted :: Bool
    , respIsUltraRestricted :: Bool
    , respIsBot :: Bool
    , respUpdated :: Int
    , respIsAppUser :: Bool
    }
  deriving (Eq, Show)

instance ToForm UserInfoRequest where
  toForm (UserInfoRequest {..}) =
    toForm [ "user" &= user ]

instance FromJSON UserInfoResponse where
  parseJSON = withObject "UserInfoResponse" $ \envelope -> do
    obj <- envelope .: "user"
    UserInfoResponse
      <$> obj .: "id"
      <*> obj .: "team_id"
      <*> obj .: "name"
      <*> obj .: "deleted"
      <*> obj .: "color"
      <*> obj .: "real_name"
      <*> obj .: "tz"
      <*> obj .: "tz_label"
      <*> obj .: "tz_offset"
      <*> obj .: "is_admin"
      <*> obj .: "is_owner"
      <*> obj .: "is_primary_owner"
      <*> obj .: "is_restricted"
      <*> obj .: "is_ultra_restricted"
      <*> obj .: "is_bot"
      <*> obj .: "updated"
      <*> obj .: "is_app_user"

instance WebAPIEndpoint Users_Info where
  type InputType Users_Info = UserInfoRequest
  type InputContentType Users_Info = URLFormEncoded
  type OutputType Users_Info = UserInfoResponse
  type OutputContentType Users_Info = JSON

  endpoint _ = "users.info"
