{-# LANGUAGE OverloadedStrings #-}

{-|
Module      : Chat.Slackboop.API.ChatPostMessage
Description : Posting messages. Duh.

This module is meant to be imported qualified.
-}

module Chat.Slackboop.API.ChatPostMessage
  ( Chat_PostMessage
  , Message(..), MessageResponse(..)
  )
where

import qualified Prelude ()
import Prologue

import Data.Aeson

import Chat.Slackboop.API

-- | Post a public message to a channel that everyone can see.
--   See also `Chat_PostEphemeral'.
data Chat_PostMessage

type ChannelID = Text
type Timestamp = Text

data Message =
  Message
    { channel :: ChannelID
    , text :: Text
    , asUser :: Bool
    }
  deriving (Eq, Show)

data MessageResponse =
  MessageResponse
    { respChannel :: ChannelID
    , respTimestamp :: Timestamp
    }
  deriving (Eq, Show)

instance ToJSON Message where
  toJSON (Message { channel, text, asUser }) =
    object [ "channel" .= channel
           , "text" .= text
           , "as_user" .= asUser
           ]

instance FromJSON MessageResponse where
  parseJSON = withObject "MessageResponse" $ \obj -> MessageResponse
    <$> obj .: "channel"
    <*> obj .: "ts"

instance WebAPIEndpoint Chat_PostMessage where
  type InputType Chat_PostMessage = Message
  type InputContentType Chat_PostMessage = JSON
  type OutputType Chat_PostMessage = MessageResponse
  type OutputContentType Chat_PostMessage = JSON

  endpoint _ = "chat.postMessage"
