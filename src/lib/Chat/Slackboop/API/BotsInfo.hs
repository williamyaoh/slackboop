{-|
Module      : Chat.Slackboop.API.BotsInfo
Description : Detailed information about Slack bots.

This module is meant to be imported qualified.
-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Chat.Slackboop.API.BotsInfo
  ( Bots_Info
  , BotInfoRequest(..), BotInfoResponse(..)
  )
where

import qualified Prelude ()
import Prologue

import Data.Aeson
import Web.FormUrlEncoded

import Chat.Slackboop.API
import Chat.Slackboop.API.Types ( (&=) )

-- | Look up detailed information for a specific bot.
data Bots_Info

type BotID = Text
type AppID = Text

data BotInfoRequest =
  BotInfoRequest
    -- TODO: Technically, this ID is optional.
    { bot :: BotID }
  deriving (Eq, Show)

data BotInfoResponse =
  BotInfoResponse
    { respBotID :: BotID
    , respBotName :: Text
    , respBotDeleted :: Bool
    , respBotUpdated :: Int
    , respBotAppID :: AppID
    -- TODO: missing: `icons'
    }
  deriving (Eq, Show)

instance ToForm BotInfoRequest where
  toForm (BotInfoRequest {..}) =
    toForm [ "bot" &= bot ]

instance FromJSON BotInfoResponse where
  parseJSON = withObject "BotInfoResponse" $ \envelope -> do
    obj <- envelope .: "bot"
    BotInfoResponse
      <$> obj .: "id"
      <*> obj .: "name"
      <*> obj .: "deleted"
      <*> obj .: "updated"
      <*> obj .: "app_id"

instance WebAPIEndpoint Bots_Info where
  type InputType Bots_Info = BotInfoRequest
  type InputContentType Bots_Info = URLFormEncoded
  type OutputType Bots_Info = BotInfoResponse
  type OutputContentType Bots_Info = JSON

  endpoint _ = "bots.info"
