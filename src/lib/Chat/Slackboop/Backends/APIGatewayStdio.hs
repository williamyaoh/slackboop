{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

-- |
-- Module      : Chat.Slackboop.Backends.APIGatewayStdio
-- Description : Pull Slack requests from stdio as JSON. For use with AWS Lambda.
-- Copyright   : (c) 2018 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : POSIX
--
-- Instead of having to actually administer servers, it's nicer to be able
-- to drop our Slack application on a serverless infrastructure and have it
-- "just work."
--
-- Here we provide a way to run a Slack app on such an infrasture, AWS Lambda
-- combined with AWS API Gateway to send HTTP requests to our Lambda function.
-- We fork a Haskell process and read the incoming request from @stdin@, writing
-- a response to @stdout@. A more performant implementation might launch a
-- long-running Haskell process and communicate with it using sockets/pipes.

module Chat.Slackboop.Backends.APIGatewayStdio
  ( mkSlackboopAPIGatewayStdio )
where

import qualified Prelude ( read )
import Prologue

import Data.Aeson
import Data.Aeson.Text
import Data.Text.Encoding
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Builder as BL

import Web.FormUrlEncoded

import System.IO ( stdin, stdout, stderr )
import qualified System.IO as IO

import Control.Monad hiding ( fail )
import Control.Exception

import Chat.Slackboop.Backends.Types
import Chat.Slackboop.Bot.Verification
import Chat.Slackboop.SlashCommands.Types

data APIGatewayIncoming =
  APIGatewayIncoming
    { incomingResource :: Text
    , incomingMethod :: Text
    , incomingSlackRequestTimestamp :: Int
    , incomingSlackSignature :: Text
    , incomingBody :: BL.ByteString
    }
  deriving (Eq, Show)

instance FromJSON APIGatewayIncoming where
  parseJSON = withObject "APIGatewayIncoming" $ \apig -> do
    headers <- apig .: "headers"
    APIGatewayIncoming
      <$> apig .: "resource"
      <*> apig .: "httpMethod"
      <*> (fmap (Prelude.read . unpack) (headers .: "X-Slack-Request-Timestamp"))
      <*> headers .: "X-Slack-Signature"
      <*> (fmap (BL.toLazyByteString . encodeUtf8Builder) (apig .: "body"))

ingestAPIG :: IO APIGatewayIncoming
ingestAPIG = do
  contents <- BL.hGetContents stdin
  case eitherDecode @APIGatewayIncoming contents of
    Left reason -> fail reason
    Right apig -> pure apig

verifySlackSignature :: Text -> APIGatewayIncoming -> IO ()
verifySlackSignature signingSecret apig =
  let body = BL.toStrict $ incomingBody apig
      signInput = SlackSignInput body (incomingSlackRequestTimestamp apig)
      signed = computeSlackSignatureHeader signingSecret signInput
  in when (signed /= incomingSlackSignature apig) $ fail
       "could not verify Slack signature header, did you specify the wrong signing secret?"

verifyMethod :: APIGatewayIncoming -> IO ()
verifyMethod apig =
  when (incomingMethod apig /= "POST") $ fail "got something which wasn't a POST request"

handleEventAPIG :: (Value -> IO (Maybe Value)) -> BL.ByteString -> IO (Maybe Value)
handleEventAPIG body input = do
  case eitherDecode @Value input of
    Left reason -> fail reason
    Right value -> body value

handleItxnAPIG :: (Value -> IO ()) -> BL.ByteString -> IO (Maybe Value)
handleItxnAPIG body input = do
  case urlDecodeAsForm @ItxnPayload input of
    Left reason -> fail $ unpack reason
    Right payload -> body (itxnPayload payload) >> pure Nothing

handleSlashCmdAPIG :: (SlashCmdData -> IO ()) -> BL.ByteString -> IO (Maybe Value)
handleSlashCmdAPIG body input = do
  case urlDecodeAsForm @SlashCmdData input of
    Left reason -> fail $ unpack reason
    Right cmd -> body cmd >> pure Nothing

mkAPIGBody :: Text -> SlackHandlers -> IO (Maybe Value)
mkAPIGBody signingSecret handlers = do
  apig <- ingestAPIG
  verifySlackSignature signingSecret apig
  verifyMethod apig
  let SlackHandlers {..} = handlers
  let APIGatewayIncoming {..} = apig
  if | incomingResource == "/event" -> handleEventAPIG onEvent incomingBody
     | incomingResource == "/interaction" -> handleItxnAPIG onItxn incomingBody
     | incomingResource == "/slashcmd" -> handleSlashCmdAPIG onSlashCmd incomingBody
     | otherwise -> fail "unknown endpoint"

mkSlackboopAPIGatewayStdio :: Text -> SlackHandlers -> IO ()
mkSlackboopAPIGatewayStdio signingSecret handlers = do
  result <- try @SomeException $ mkAPIGBody signingSecret handlers
  response <- case result of
    Right Nothing -> pure $ mkResponse 200 ""
    Right (Just val) -> pure $ mkResponse 200 $ encodeToLazyText val
    Left exn -> do
      IO.hPutStrLn stderr $ show exn
      pure $ mkResponse 400 ""
  BL.hPutStr stdout $ encode response
  where mkResponse :: Int -> TL.Text -> Value
        mkResponse code body =
          object [ "body" .= body
                 , "statusCode" .= code
                 , "headers" .= object []
                 , "isBase64Encoded" .= False
                 ]
