{-# OPTIONS -Wno-orphans #-}
{-# LANGUAGE OverloadedStrings #-}

module Chat.Slackboop.Backends.Types
  ( SlackHandlers(..)
  , ItxnPayload(..)
  )
where

import qualified Prelude ()
import Prologue

import Data.Aeson
import Data.String.Interpolate ( i )
import Web.FormUrlEncoded
import Web.Internal.HttpApiData

import Chat.Slackboop.SlashCommands.Types

-- | All the functions we actually need to run a full Slack application.
--   There's not a lot of constraints on the types here, so you probably
--   shouldn't be using this directly, but... if you need to, it's here.
data SlackHandlers =
  SlackHandlers
    { onEvent :: Value -> IO (Maybe Value)
    , onItxn :: Value -> IO ()
    , onSlashCmd :: SlashCmdData -> IO ()
    }

-- | When Slack sends us a the result of an interactive component, for
--   some reason it's JSON... encoded in form data. So we need to unwrap it.
data ItxnPayload =
  ItxnPayload
    { itxnPayload :: Value }
  deriving (Eq, Show)

instance FromForm ItxnPayload where
  fromForm f = ItxnPayload <$> parseUnique "payload" f

instance FromHttpApiData Value where
  parseQueryParam t =
    let bs = [i|#{t}|]
    in case decode bs of
         Nothing -> Left "couldn't get JSON value"
         Just value -> Right value

instance FromForm SlashCmdData where
  fromForm f = SlashCmdData
    <$> parseUnique "command" f
    <*> parseUnique "text" f
    <*> parseUnique "user_id" f
    <*> parseUnique "user_name" f
    <*> parseUnique "team_id" f
    <*> parseUnique "team_domain" f
    <*> parseUnique "response_url" f
    <*> parseUnique "channel_id" f
    <*> parseUnique "channel_name" f
