{-# OPTIONS -Wno-orphans #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Chat.Slackboop.Backends.Localhost
  ( SlackboopLocalServer
  , mkSlackboopLocalServer, slackboopLocalProxy
  )
where

import qualified Prelude ()
import Prologue

import Data.Aeson
import Data.String.Interpolate ( i )
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as LB

import Web.FormUrlEncoded

import Servant as S
import Servant.API.ContentTypes ()

import Chat.Slackboop.Bot.Verification
import Chat.Slackboop.SlashCommands.Types

type RequiredHeader =
  Header' '[Required, Strict]

type WithSlackHeaders a =
  RequiredHeader "X-Slack-Signature" Text :> RequiredHeader "X-Slack-Request-Timestamp" Int :> a

type EventEndpoint =
  WithSlackHeaders (ReqBody '[JSON] LB.ByteString :> Post '[JSON] (MaybeContent Value))
type InteractionEndpoint =
  WithSlackHeaders (ReqBody '[FormUrlEncoded] LB.ByteString :> Post '[JSON] NoContent)
type SlashCmdEndpoint =
  WithSlackHeaders (ReqBody '[FormUrlEncoded] LB.ByteString :> Post '[JSON] NoContent)

type SlackboopLocalServer =
  "event" :> EventEndpoint :<|>
  "interactive" :> InteractionEndpoint :<|>
  "slashcmd" :> SlashCmdEndpoint

data Interaction =
  Interaction
    { interactionPayload :: Value }
  deriving (Eq, Show)

-- | Unfortunately, Servant doesn't seem to provide a way to have an endpoint
--   that can both return content and not return content. So we need a wrapper.
data MaybeContent a
  = NothingContent
  | JustContent a
  deriving (Eq, Show)

-- Note to self: ask if there's a way to collapse all of these MimeRender
-- instances without running into overlapping issues...

-- It's okay that these overlap, since they're fully specific (this will be
-- chosen every time.) This is kind of a shim or hack; we've short-circuited
-- Servant's machinery for parsing incoming JSON values so that we can
-- directly use the bytes when the `Content-Type' is `application/json'.

instance {-# OVERLAPS #-} MimeUnrender JSON B.ByteString where
  mimeUnrender _ bs = Right $ LB.toStrict bs

instance {-# OVERLAPS #-} MimeUnrender JSON LB.ByteString where
  mimeUnrender _ bs = Right bs

instance {-# OVERLAPS #-} MimeUnrender FormUrlEncoded B.ByteString where
  mimeUnrender _ bs = Right $ LB.toStrict bs

instance {-# OVERLAPS #-} MimeUnrender FormUrlEncoded LB.ByteString where
  mimeUnrender _ bs = Right bs

instance MimeRender JSON a => MimeRender JSON (MaybeContent a) where
  mimeRender _ NothingContent = ""
  mimeRender p (JustContent val) = mimeRender p val

instance MimeRender PlainText a => MimeRender PlainText (MaybeContent a) where
  mimeRender _ NothingContent = ""
  mimeRender p (JustContent val) = mimeRender p val

instance MimeRender FormUrlEncoded a => MimeRender FormUrlEncoded (MaybeContent a) where
  mimeRender _ NothingContent = ""
  mimeRender p (JustContent val) = mimeRender p val

instance MimeRender OctetStream a => MimeRender OctetStream (MaybeContent a) where
  mimeRender _ NothingContent = ""
  mimeRender p (JustContent val) = mimeRender p val

instance FromForm Interaction where
  fromForm f = Interaction <$> parseUnique "payload" f

instance FromHttpApiData Value where
  parseQueryParam t =
    let bs = [i|#{t}|]
    in case decode bs of
         Nothing -> Left "couldn't get JSON value"
         Just value -> Right value

instance FromForm SlashCmdData where
  fromForm f = SlashCmdData
    <$> parseUnique "command" f
    <*> parseUnique "text" f
    <*> parseUnique "user_id" f
    <*> parseUnique "user_name" f
    <*> parseUnique "team_id" f
    <*> parseUnique "team_domain" f
    <*> parseUnique "response_url" f
    <*> parseUnique "channel_id" f
    <*> parseUnique "channel_name" f

-- | Augment a handler with extra code to verify that the request is
--   actually coming from Slack, and reject it if it isn't.
guardSlackSignature :: MonadIO m
                    => Text
                    -> (LB.ByteString -> IO a)
                    -> (Text -> Int -> LB.ByteString -> m a)
guardSlackSignature key handler signature timestamp payload =
  liftIO $ if expectedHeader /= signature
    then fail "failed to verify Slack signature"
    else handler payload

  where signInput :: SlackSignInput
        signInput = SlackSignInput
          { signPayload = LB.toStrict payload, signTimestamp = timestamp }

        expectedHeader :: Text
        expectedHeader = computeSlackSignatureHeader key signInput

mkSlackboopLocalServer :: Text
                       -> (Value -> IO (Maybe Value))
                       -> (Value -> IO ())
                       -> (SlashCmdData -> IO ())
                       -> Server SlackboopLocalServer
mkSlackboopLocalServer signingSecret onEvent onInteraction onSlashCmd =
  eventServer :<|> interactionServer :<|> slashCmdServer
  where eventServer = guardSlackSignature signingSecret $ \payload ->
          liftIO $ case eitherDecode @Value payload of
            Left reason -> fail reason
            Right value -> do
              ret <- onEvent value
              case ret of
                Nothing -> pure NothingContent
                Just retval -> pure $ JustContent retval

        interactionServer = guardSlackSignature signingSecret $ \payload ->
          case urlDecodeAsForm @Interaction payload of
            Left reason -> fail $ unpack reason
            Right interaction -> do
              onInteraction (interactionPayload interaction)
              pure NoContent

        slashCmdServer = guardSlackSignature signingSecret $ \payload ->
          case urlDecodeAsForm @SlashCmdData payload of
            Left reason -> fail $ unpack reason
            Right cmd -> do
              onSlashCmd cmd
              pure NoContent

slackboopLocalProxy :: Proxy SlackboopLocalServer
slackboopLocalProxy =
  Proxy
