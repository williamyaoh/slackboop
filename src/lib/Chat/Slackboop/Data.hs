-- |
-- Module      : Chat.Slackboop.Data
-- Description : Utilities for dealing with Slack data.
-- Copyright   : (c) 2019 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : GHC
--
-- Slack has a number of important data structures, such as the format they
-- have for user IDs/channel IDs that are sent in messages to your bot.
--
-- This module contains utilities for extracting and working with said data.

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Chat.Slackboop.Data
  ( extractUserID, userIDRegex )
where

import Prelude  ()
import Prologue

import Data.Maybe ( mapMaybe )

import Text.RawString.QQ    ( r )
import Text.Regex.TDFA
import Text.Regex.TDFA.Text ()

import Chat.Slackboop.Types

-- $messages
--
-- While the basic message type from Slack isn't that complicated, it gets
-- more complicated when you start wanting to add attachments, menus, and buttons.

-- $ids
--
-- In your actual message, Slack will send you user IDs in the format
-- @<\@userid>@, or @<\@userid|username>@, if you enable username escaping.

-- |
-- Given some text, extract all the user IDs (and optional associated usernames),
-- in the order that they appear.
extractUserID :: Text -> [(UserID, Maybe UserName)]
extractUserID str =
  let possible = getAllTextMatches $ str =~ possibleRegex
  in mapMaybe extractSingle possible
  where possibleRegex :: Text
        possibleRegex = "<[^>]+>"

        extractSingle :: Text -> Maybe (UserID, Maybe UserName)
        extractSingle str = case getAllTextSubmatches $ str =~ userIDRegex of
          [_, userID, _, ""]       -> Just (UserID userID, Nothing)
          [_, userID, _, userName] -> Just (UserID userID, Just $ UserName userName)
          _                        -> Nothing

-- |
-- Regex for extracting the user ID (and optional username) from Slack messages.
userIDRegex :: Text
userIDRegex = [r|<@([A-Z0-9]+)(\|([a-zA-Z0-9\-_]+))?>|]
