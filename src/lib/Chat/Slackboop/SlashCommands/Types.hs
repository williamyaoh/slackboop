module Chat.Slackboop.SlashCommands.Types
  ( SlashCmdData(..) )
where

import qualified Prelude ()
import Prologue

-- | Slack apps can add commands to Slack itself, which, when entered by
--   a user, don't get posted to the current channel and instead get sent
--   directly to the app. An example would be `/slackboop do something'.
data SlashCmdData =
  SlashCmdData
    { cmdCommand :: Text
    , cmdText :: Text
    , cmdUserID :: Text
    , cmdUserName :: Text
    , cmdTeamID :: Text
    , cmdTeamDomain :: Text
    , cmdResponseURL :: Text
    , cmdChannelID :: Text
    , cmdChannelName :: Text
    }
  deriving (Eq, Show)
