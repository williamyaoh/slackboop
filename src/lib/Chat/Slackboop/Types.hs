-- |
-- Module      : Chat.Slackboop.Types
-- Description : Types for Slack data
-- Copyright   : (c) William Yao, 2019
-- License     : BSD-3
-- Maintainer  : williamyaoh@gmail.com
-- Stability   : experimental
-- Portability : POSIX

{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Chat.Slackboop.Types
  (
  -- * Types
  -- $types
    UserID(..), UserName(..),
    BotID(..),
    ChannelID(..), ChannelName(..),
    TeamID(..), TeamName(..)
  )
where

import qualified Prelude ()
import Prologue

import Data.String ( IsString )
import Data.Aeson ( FromJSON, ToJSON )

-- $types
--
-- Note that every bot user has both a user ID and a bot ID, which are not
-- interchangeable. Unfortunately, some API methods require user IDs, while
-- others require bot IDs.

newtype UserID = UserID { unUserID :: Text }
  deriving (Eq, Show, IsString)

newtype UserName = UserName { unUserName :: Text }
  deriving (Eq, Show, IsString)

newtype BotID = BotID { unBotID :: Text }
  deriving (Eq, Show, IsString)

newtype ChannelID = ChannelID { unChannelID :: Text }
  deriving (Eq, Show, IsString)

newtype ChannelName = ChannelName { unChannelName :: Text }
  deriving (Eq, Show, IsString)

newtype TeamName = TeamName { unTeamName :: Text }
  deriving (Eq, Show, IsString)

newtype TeamID = TeamID { unTeamID :: Text }
  deriving (Eq, Show, IsString)

deriving newtype instance FromJSON UserID
deriving newtype instance FromJSON UserName
deriving newtype instance FromJSON BotID
deriving newtype instance FromJSON ChannelID
deriving newtype instance FromJSON ChannelName
deriving newtype instance FromJSON TeamID
deriving newtype instance FromJSON TeamName

deriving newtype instance ToJSON UserID
deriving newtype instance ToJSON UserName
deriving newtype instance ToJSON BotID
deriving newtype instance ToJSON ChannelID
deriving newtype instance ToJSON ChannelName
deriving newtype instance ToJSON TeamID
deriving newtype instance ToJSON TeamName
