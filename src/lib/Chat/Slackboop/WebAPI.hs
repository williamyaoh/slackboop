-- |
-- Module      : Chat.Slackboop.WebAPI
-- Description : Sending calls to Slack's API
-- Copyright   : (c) 2019 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : POSIX

module Chat.Slackboop.WebAPI
  ( module Chat.Slackboop.WebAPI.Raw
  , module Chat.Slackboop.WebAPI.Typed
  )
where

import Prelude ()

import Chat.Slackboop.WebAPI.Raw
import Chat.Slackboop.WebAPI.Typed
