-- |
-- Module      : Chat.Slackboop.Bot
-- Description : Running a server to ingest incoming Slack events
-- Copyright   : (c) 2019 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : GHC

module Chat.Slackboop.Bot
  ( module Chat.Slackboop.Bot.Types
  , module Chat.Slackboop.Bot.Server
  , module Chat.Slackboop.Bot.Handlers
  )
where

import Prelude ()

import Chat.Slackboop.Bot.Types
import Chat.Slackboop.Bot.Server
import Chat.Slackboop.Bot.Handlers
