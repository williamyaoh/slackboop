-- |
-- Module      : Chat.Slackboop.Bot.Handlers
-- Description : Tools for defining handlers
-- Copyright   : (c) 2019 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : GHC
--
-- Tools for defining handlers.

{-# LANGUAGE Rank2Types #-}

module Chat.Slackboop.Bot.Handlers
  ( eventHandler, hoistEventHandler, eventParseHandler, hoistEventParseHandler )
where

import Prelude  ()
import Prologue

import Data.Aeson
import Data.Aeson.Types

import Chat.Slackboop.Bot.Types

-- |
-- Convenience function for the usual case where you simply want to run some
-- action in response to an incoming Event. Runs the given action in the
-- background to avoid timeout issues with Slack.
eventHandler :: FromJSON a => (a -> IO ()) -> EventHandler
eventHandler = hoistEventHandler id

-- |
-- Like `eventHandler', but allows for the usage of an arbitrary monad stack.
hoistEventHandler :: FromJSON a => (forall b. m b -> IO b) -> (a -> m ()) -> EventHandler
hoistEventHandler transform body = \obj -> case fromJSON $ Object obj of
  Error _ -> pure Nothing
  Success input -> pure $ Just $
    pure ((), transform $ body input)

-- |
-- Like `eventHandler', but allows for the direct usage of a `Parser'.
-- This means that you don't need to define a data type for writing a handler.
-- So you could do:
--
-- > eventParseHandler (\obj -> (,) <$> obj .: "foo" <*> obj .: "bar") $
-- >   \foo -> do
-- >     {- ... -}
--
-- and directly get the attributes without needing an intermediate data type.
eventParseHandler :: (Object -> Parser a) -> (a -> IO ()) -> EventHandler
eventParseHandler = hoistEventParseHandler id

-- |
-- Combines the capabilities of `hoistEventHandler' and `eventParseHandler'.
hoistEventParseHandler :: (forall b. m b -> IO b)
                       -> (Object -> Parser a)
                       -> (a -> m ())
                       -> EventHandler
hoistEventParseHandler transform parser body = \obj -> case parseEither parser obj of
  Left _ -> pure Nothing
  Right input -> pure $ Just $
    pure ((), transform $ body input)
