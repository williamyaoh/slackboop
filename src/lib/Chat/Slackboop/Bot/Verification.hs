-- |
-- Module      : Chat.Slackboop.Bot.Verification
-- Description : Verifying that incoming HTTP requests are coming from Slack.
-- Copyright   : (c) 2018-2019 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : GHC
--
-- Slack sends HTTP POST requests to us when events happen in the Slack server:
-- someone posts a message, someone DMs us, etc. Obviously we want to respond to
-- these, but we also need to make sure that Slack is the one sending us those
-- events, and that they're not being spoofed by some malicious third party in
-- order to get our Slack app to do bad things.
--
-- To do this, we compute a signed hash of the request body (specifically, a
-- SHA256 HMAC), and compare it to one of the request headers.
--
-- See <https://api.slack.com/docs/verifying-requests-from-slack> for the
-- specifics of how this works.

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Chat.Slackboop.Bot.Verification
  (
  -- * Usage
  -- $usage
    SlackSignInput(..)
  , computeSlackSignatureHeader
  -- * Internal functions
  , computeSlackSignatureRaw
  , toHex
  )
where

import qualified Prelude  ()
import           Prologue

import Crypto.Hash.Algorithms ( SHA256 )
import Crypto.MAC.HMAC

import Data.ByteString         ( ByteString )
import Data.String.Interpolate ( i )

-- $usage
--
-- To use this module, feed `computeSlackSignatureHeader' the data from the
-- Slack request, and compare the result to what's in the @X-Slack-Signature@
-- header. If it matches, you're good to go. Otherwise, someone's trying to
-- spoof you.

-- |
-- We need three pieces of information from the request in order to
-- verify that it's coming from Slack: the HTTP body bytes themselves,
-- the timestamp value of the @X-Slack-Request-Timestamp@ header, and the
-- version of Slack's signing protocol. Currently the protocol is always
-- @v0@, so there's no need to specify it here.
data SlackSignInput =
  SlackSignInput
    { signPayload   :: ByteString
    , signTimestamp :: Int
    }
  deriving (Eq, Show)

-- |
-- The bytes on which the HMAC are computed is a concatenation of the signing
-- protocol version, the timestamp, and the body bytes themselves.
computeSignableBytes :: SlackSignInput -> ByteString
computeSignableBytes input =
  [i|v0:#{tsByterep}:#{signPayload input}|]
  where tsByterep :: ByteString
        tsByterep = [i|#{signTimestamp input}|]  -- converts to UTF8

-- |
-- We don't expect to use this directly in other library modules, but it's
-- useful to provide for testing/transparency.
computeSlackSignatureRaw :: Text -> SlackSignInput -> HMAC SHA256
computeSlackSignatureRaw key payload =
  hmac keyBytes payloadBytes
  where keyBytes :: ByteString
        keyBytes = [i|#{key}|]

        payloadBytes :: ByteString
        payloadBytes = computeSignableBytes payload

-- |
-- Given a signing key and a signable Slack payload, compute
-- the expected value of the @X-Slack-Signature@ header.
computeSlackSignatureHeader :: Text -> SlackSignInput -> Text
computeSlackSignatureHeader key payload =
  [i|v0=#{hexDigest}|]
  where hexDigest :: Text
        hexDigest = toHex $ computeSlackSignatureRaw key payload

-- |
-- It's slightly annoying that @cryptonite@ doesn't provide this already.
-- Convert the digest bytes into a hexadecimal string.
toHex :: HMAC a -> Text
toHex digest = [i|#{hmacGetDigest digest}|]
