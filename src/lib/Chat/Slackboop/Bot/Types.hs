-- |
-- Module      : Chat.Slackboop.Bot.Types
-- Copyright   : (c) 2019 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : GHC

{-# LANGUAGE QuasiQuotes #-}

module Chat.Slackboop.Bot.Types where

import qualified Prelude
import           Prologue

import Data.Aeson
import Data.String             ( IsString )
import Data.String.Interpolate ( i )

import Network.Wai as Wai

-- |
-- Unfortunately, the world isn't perfect, and we have to deal with errors
-- from incoming events. Some examples of errors that might occur:
--
--  * The incoming request isn't actually from Slack
--  * The incoming request hit a nonexistent endpoint
--  * The incoming request isn't valid JSON
data SlackWebhookError
  = TransportLayerError Wai.Request Wai.Response  -- ^ Something went wrong at the HTTP layer.
  | VerificationFailed  -- ^ The incoming request isn't coming from Slack.
  | DifferentApp  -- ^ The incoming request is for a different Slack app than us.
  | InvalidPayload Prelude.String  -- ^ We couldn't parse the payload. It's probably not JSON.
  | HandlerMissing Object  -- ^ There's no handler registered for this event.
  | DuplicateWebhook Object  -- ^ This isn't the first time we've seen something.

-- |
-- Each event type (Event API, Slash Command, Interaction API) seems to have its
-- own separate ID type, but whatever... we'll just abstract over it with this type.
newtype EventID = EventID { unEventID :: Text }
  deriving (Eq, Ord, Show, IsString)

data Action = Respond | Ignore
  deriving (Eq, Show)

type ErrorHandler = SlackWebhookError -> IO ()

-- |
-- Slack is pretty stringent on timeouts for events. If the webhook doesn't
-- respond with an HTTP 200 within 3 seconds for the Event API, and 300ms for
-- slash command handlers, Slack considers the event to have failed delivery
-- and resends it 3 times. This is unfortunate, because it can mean that sheer
-- bad luck can result in your bot getting sent duplicate events, and thus
-- responding more than necessary! So your bot needs to have a way to detect
-- when an incoming event has already been responded to and ignore it.
type DuplicateDetector = EventID -> IO Action

-- |
-- Needed to make sure that requests are actually coming from Slack and not
-- some third party.
data VerificationInfo = VerificationInfo
  { slackSigningSecret :: Text
  , slackAppID         :: Text
  }
  deriving (Eq, Show)

data SlackboopConfig = SlackboopConfig
  { errorHandler      :: ErrorHandler
  , duplicateDetector :: DuplicateDetector
  }

data Slackboop = Slackboop
  { eventHandlers :: [EventHandler]
  }

instance Show SlackWebhookError where
  show (TransportLayerError request response) =
    [i|TransportLayerError (#{request}) (#{responseStatus response})|]
  show VerificationFailed =
    [i|VerificationFailed|]
  show DifferentApp =
    [i|DifferentApp|]
  show (InvalidPayload request) =
    [i|InvalidPayload (#{request})|]
  show (HandlerMissing obj) =
    [i|HandlerMissing (#{obj})|]
  show (DuplicateWebhook obj) =
    [i|DuplicateWebhook (#{obj})|]

-- |
-- Don't panic! The type for this looks really scary, but trust me, it's all
-- for a reason. Here's a diagram:
--
-- @
-- type WebhookHandler i o
--                     ^ ^
--                     | |__ what the handler returns in the HTTP body
--                     |__ what the handler takes in from Slack's request body
--
-- WebhookHandler (i -> IO (Maybe (IO (o, IO ()))))
--                       ^          ^      ^
--                       |          |      |__ background processing for the response
--                       |          |__ immediate response to Slack
--                       |__ whether or not to handle this specific input
-- @
--
-- The reason it's structured this way is that first, we need to decide whether
-- this handler should even apply to the given input; second, we need to
-- /immediately/ return something back to Slack (because of the Slack's stringent
-- timeouts, see `DuplicateDetector'), and finally we need to do something
-- asynchronously in the background to actually respond to the event.
--
-- If you're writing a handler for some Slack event, best practice is to do the
-- __minimal__ amount of processing needed to figure out what to return to Slack,
-- and then do all the heavy lifting in the background, by returning something
-- in the innermost IO.
type WebhookHandler i o = i -> IO (Maybe (IO (o, IO ())))

type EventHandler = WebhookHandler Object ()
