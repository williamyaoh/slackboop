{-# OPTIONS -Wno-orphans #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Chat.Slackboop.Bot.Server.Orphans where

import Prelude  ()
import Prologue

import Data.ByteString.Lazy as LB

import Servant

instance {-# OVERLAPS #-} MimeRender JSON LB.ByteString where
  mimeRender _ bs = bs
instance {-# OVERLAPS #-} MimeUnrender JSON LB.ByteString where
  mimeUnrender _ bs = Right bs
