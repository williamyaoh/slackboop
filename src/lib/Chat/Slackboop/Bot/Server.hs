-- |
-- Module      : Chat.Slackboop.Bot.Server
-- Description : Spin up a server to ingest incoming Slack events
-- Copyright   : (c) 2019 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portability : GHC

{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE NamedFieldPuns       #-}
{-# LANGUAGE OverloadedLists      #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE QuasiQuotes          #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeApplications     #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE UndecidableInstances #-}

module Chat.Slackboop.Bot.Server where

import qualified Prelude
import           Prologue

import           Data.Aeson
import qualified Data.Aeson              as JSON
import           Data.Aeson.Types        ( Parser, parseEither )
import           Data.ByteString.Lazy    as LB
import           Data.Set                as Set
import           Data.String.Interpolate ( i )

import Control.Concurrent      ( forkIO )
import Control.Concurrent.MVar
import Control.Monad           ( when )

import qualified System.IO        as IO
import           System.IO.Unsafe ( unsafePerformIO )

import Network.HTTP.Types
import Network.Wai        as Wai

import Servant

import Chat.Slackboop.Bot.Server.Orphans ()
import Chat.Slackboop.Bot.Types
import Chat.Slackboop.Bot.Verification

-- $serving
--
-- To turn a set of handlers into a Slack bot:
--
-- > Warp.run 8000 $ serveBot (VerificationInfo { .. }) slackHandlers
--
-- If you need to customize the error handling or the duplicate detection,
-- use `serveBotWithConfig'.

-- | Just print any errors to `IO.stderr'.
defaultErrorHandler :: ErrorHandler
defaultErrorHandler = IO.hPutStrLn IO.stderr . show

duplicateSet :: MVar (Set.Set EventID)
duplicateSet = unsafePerformIO $ newMVar Set.empty  -- This is safe.

-- |
-- The default duplicate detector just uses an in-memory `Set' to keep track of
-- which messages have been seen. Since Slack only resends events for up to
-- 5 minutes, this seems like a reasonable choice.
defaultDuplicateDetector :: DuplicateDetector
defaultDuplicateDetector eventID = do
  currentDuplicates <- takeMVar duplicateSet
  let result = if Set.member eventID currentDuplicates then Ignore else Respond
  putMVar duplicateSet $ Set.insert eventID currentDuplicates
  pure result

defaultSlackboopConfig :: SlackboopConfig
defaultSlackboopConfig = SlackboopConfig
  { errorHandler = defaultErrorHandler
  , duplicateDetector = defaultDuplicateDetector
  }

serveBot :: VerificationInfo -> Slackboop -> Wai.Application
serveBot = serveBotWithConfig defaultSlackboopConfig

serveBotWithConfig :: SlackboopConfig -> VerificationInfo -> Slackboop -> Wai.Application
serveBotWithConfig config verif slackboop =
  interceptHTTPErrors errorcc $ serve (Proxy @WebhookAPI) slackbot
  where slackbot :: Server WebhookAPI
        slackbot = eventsHandler

        eventsHandler :: Text -> Int -> LB.ByteString -> Handler LB.ByteString
        eventsHandler sig ts =
          guardSlackSignature errorcc verif sig ts $
            guardJSONParse errorcc $
              guardAppID errorcc verif $
                guardDuplicates errorcc duplicateDetector $
                  compileEventHandlers errorcc compiledEventHandlers

        compiledEventHandlers :: [Object -> Handler (Maybe LB.ByteString)]
        compiledEventHandlers =
            compileHandler challengeHandler
          : compileHandler `fmap` eventHandlers

        SlackboopConfig { errorHandler = errorcc, duplicateDetector } = config
        Slackboop { eventHandlers } = slackboop

-- $events
--
-- Slack @Event@s are specifically caused by interactions and messages posted
-- in the Slack workspace; they're distinct from slash commands and interactions
-- (clicking on buttons/menus that your bot provides), which Slack sends in
-- a different format. Throughout this documentation, we use uppercase Event to
-- refer specifically to Slack Events as distinct from the other two webhooks
-- that Slack sends, and lowercase event to refer to all three types of webhooks.
--
-- Every Event has some structure in common: <https://api.slack.com/types/event>
--
-- In particular, every Event has an @event_id@, which can be used to avoid
-- responding to an Event multiple times, and the inner data contains an
-- @event_ts@ timestamp (as Unix time + subseconds) about when the event was
-- originally triggered (not when it was received).

compileEventHandlers :: MonadIO io
                     => ErrorHandler
                     -> [Object -> io (Maybe LB.ByteString)]
                     -> Object
                     -> io LB.ByteString
compileEventHandlers errorcc handlers input = case handlers of
  [] -> do
    liftIO $ errorcc $ HandlerMissing input
    pure $ encode $ object [ "ok" .= False, "error" .= ("no_handler" :: Text) ]
  (h : hs) -> h input >>= \case
    Nothing -> compileEventHandlers errorcc hs input
    Just lb -> pure lb

type RequiredHeader = Header' '[Required, Strict]

type WebhookAPI =
     RequiredHeader "X-Slack-Signature" Text
  :> RequiredHeader "X-Slack-Request-Timestamp" Int
  :> HandlersAPI

type HandlersAPI = EventsAPI

type EventsAPI = "event" :> ReqBody '[JSON] LB.ByteString :> Post '[JSON] LB.ByteString

class Serializable a where
  serialize :: a -> LB.ByteString

instance {-# OVERLAPPABLE #-} ToJSON a => Serializable a where
  serialize = encode
instance {-# OVERLAPS #-} Serializable () where
  serialize _ = ""

compileHandler :: (Serializable o, MonadIO io)
               => WebhookHandler i o
               -> i
               -> io (Maybe LB.ByteString)
compileHandler handler input = do
  cc <- liftIO $ handler input
  case cc of
    Nothing -> pure Nothing
    Just io -> do
      (output, bg) <- liftIO $ io
      _ <- liftIO $ forkIO bg
      pure $ Just $ serialize output

-- |
-- Before Slack will actually send us events (like message postings), it sends
-- a challenge request to our endpoint containing a token. We have to respond
-- with said token or Slack will just ignore us.
challengeHandler :: WebhookHandler Object Object
challengeHandler obj = case parseEither challengeParser obj of
  Left _ -> pure Nothing
  Right challengeToken -> pure $ Just $
    pure ([ ("challenge", JSON.String challengeToken) ], pure ())
  where challengeParser :: Object -> Parser Text
        challengeParser obj = do
          ty :: Text <- obj .: "type"
          if ty /= "url_verification"
            then Prelude.fail "not a URL verification event"
            else obj .: "challenge"

-- $error-cases
--
-- Slackboop does a number of bookkeeping operations on any incoming request,
-- so that you don't have to.
--
--  * It makes sure that the incoming request is coming from Slack and not
--    some third party
--  * It makes sure that the incoming request is for /your/ app and not someone
--    else's
--  * It alerts you when an incoming request fails to parse
--
-- The various @guardX@ functions implement this functionality by wrapping
-- request handlers with extra blast-padding. Note that their signatures are
-- generalized to `MonadIO', but typically this will be specialized to Servant
-- `Handler's.

-- |
-- Given a whole bunch of relevant information, wrap an IO action with
-- error handling when the body fails to verify.
guardSlackSignature :: MonadIO io
                    => ErrorHandler
                    -> VerificationInfo
                    -> Text  -- ^ @X-Slack-Signature@
                    -> Int  -- ^ @X-Slack-Request-Timestamp@
                    -> (LB.ByteString -> io LB.ByteString)
                    -> LB.ByteString
                    -> io LB.ByteString
guardSlackSignature errorcc verif signature ts handler body =
  let expected = computeSlackSignatureHeader (slackSigningSecret verif) $
        SlackSignInput { signPayload = [i|#{body}|], signTimestamp = ts }
  in if (expected /= signature)
       then do
         liftIO $ errorcc VerificationFailed
         pure $ encode $ object [ "ok" .= False, "error" .= ("verification_failed" :: Text) ]
       else handler body

-- |
-- Make sure that the incoming body correctly parses as JSON.
guardJSONParse :: (MonadIO io, FromJSON json)
               => ErrorHandler
               -> (json -> io LB.ByteString)
               -> LB.ByteString
               -> io LB.ByteString
guardJSONParse errorcc handler body = case eitherDecode body of
  Left err -> do
    liftIO $ errorcc $ InvalidPayload err
    pure ""
  Right val -> handler val

-- |
-- Given a whole bunch of relevant information, wrap an IO action with
-- error handling when the @app_id@ in the JSON envelope is different from the one
-- we expect.
guardAppID :: MonadIO io
           => ErrorHandler
           -> VerificationInfo
           -> (Object -> io LB.ByteString)
           -> Object
           -> io LB.ByteString
guardAppID errorcc verif handler body =
  case parseEither (.:? "api_app_id") =<< pure body of
    Left error -> do
      liftIO $ errorcc $ InvalidPayload error
      pure ""
    Right (Just appID) | appID /= slackAppID verif -> do
      liftIO $ errorcc DifferentApp
      pure ""
    Right _ -> handler body

-- |
-- Given a way to detect duplicates, wrap an IO action with error handling
-- when we see a duplicate event.
guardDuplicates :: MonadIO io
                => ErrorHandler
                -> DuplicateDetector
                -> (Object -> io LB.ByteString)
                -> Object
                -> io LB.ByteString
guardDuplicates errorcc detector handler body =
  case parseEither (.: "event_id") =<< pure body of
    -- If there isn't an event_id, we can just let it through.
    Left _ -> handler body
    Right eventID -> liftIO (detector $ EventID eventID) >>= \case
      Ignore -> do
        liftIO $ errorcc $ DuplicateWebhook body
        pure ""
      Respond -> handler body


-- |
-- Servant's default application doesn't give us any logging around 4XX/5XX
-- http errors... for some ungodly reason.
interceptHTTPErrors :: ErrorHandler -> Wai.Application -> Wai.Application
interceptHTTPErrors handler app request cc = app request cc'
  where cc' :: Wai.Response -> IO Wai.ResponseReceived
        cc' response = do
          let code = statusCode $ responseStatus response
          when (code >= 400) $
            handler $ TransportLayerError request response
          cc response
