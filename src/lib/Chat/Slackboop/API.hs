{-|
Module      : Chat.Slackboop.API
Description : Type-safe requests against Slack's Web API.
-}

{-# OPTIONS -Wno-redundant-constraints #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds #-}

module Chat.Slackboop.API
  ( SlackAPI(..), webAPICall, webAPICall_
  , runSlackAPI, runSlackAPIEnv
  -- * Content types and encoding
  , ContentType(..), JSON, URLFormEncoded, OutputVerify(..)
  , WebAPIEndpoint(..), APIRequestData(..)
  -- * Internal functions
  , runWebAPIRequest, encodeEndpointInput, decodeEndpointOutput, toRequestData
  )
where

import qualified Prelude ()
import Prologue

import Data.Aeson
import Data.Aeson.Types
import Data.Proxy
import Data.String.Interpolate ( i )
import Data.Either ( either )
import Data.ByteString.Lazy ( ByteString )
import qualified Data.ByteString.Lazy as BL

import Web.FormUrlEncoded

import Network.HTTP.Client
  ( Manager, Request, RequestBody(RequestBodyLBS)
  , defaultRequest, method, secure, port, host, path, requestHeaders, requestBody
  , httpLbs, responseBody
  , newManager
  )
import Network.HTTP.Client.TLS

import System.Environment

import GHC.Exts ( Constraint )

import Control.Monad.Freer

data JSON
data URLFormEncoded

-- | Generic content type class, giving us the ability to encode/decode.
class ContentType c where
  type EncodeConstraint c :: * -> Constraint
  type DecodeConstraint c :: * -> Constraint

  contentSlug :: Proxy c -> Text
  encodeContent :: EncodeConstraint c a => Proxy c -> a -> ByteString
  decodeContent :: DecodeConstraint c a => Proxy c -> ByteString -> Maybe a

instance ContentType JSON where
  type EncodeConstraint JSON = ToJSON
  type DecodeConstraint JSON = FromJSON

  contentSlug _ = "application/json;charset=utf-8"
  encodeContent _ = encode
  decodeContent _ = decode

instance ContentType URLFormEncoded where
  type EncodeConstraint URLFormEncoded = ToForm
  type DecodeConstraint URLFormEncoded = FromForm

  contentSlug _ = "application/x-www-form-urlencoded"
  encodeContent _ = urlEncodeAsForm
  decodeContent _ = either (const Nothing) Just . urlDecodeAsForm

-- | In addition to the parsing of the output itself, we also need to
--   check Slack-specific error messages that can be returned in HTTP
--   response output.
class ContentType c => OutputVerify c where
  eitherDecodeContent :: DecodeConstraint c a => Proxy c -> ByteString -> Either Text a

instance OutputVerify JSON where
  eitherDecodeContent _ bs = do
    json <- first pack $ eitherDecode @Value bs
    json' <- first pack $ parseEither verify json
    json'' <- json'
    first pack $ parseEither parseJSON json''

    where verify :: Value -> Parser (Either Text Value)
          verify = withObject "OutputVerify_JSON" $ \obj -> do
            ok <- obj .: "ok"
            if not ok
              then do { error <- obj .: "error"; pure $ Left error }
              else pure $ Right $ Object obj

-- | Default no-op implementation to do no verification if we don't have a
--   specific handler.
instance {-# OVERLAPPABLE #-} ContentType c => OutputVerify c where
  eitherDecodeContent _ =
    maybe (Left "failed to parse output type") Right <$> decodeContent (Proxy @c)

-- | An endpoint of Slack's API.
--
--   @InputType@ and @OutputType@ are there to allow domain-specific input
--   types (say, for chat messages) instead of having to directly pass
--   JSON/forms.
--
--   Note that the @InputType@ must be convertable (by typeclass instance)
--   into the @InputContentType@, and accordingly for @OutputType@/@OutputContentType@.
--   Those constraints are enforced by all the functions which use instances
--   of @WebAPIEndpoint@.
class WebAPIEndpoint endpoint where
  type InputType endpoint
  type InputContentType endpoint
  type OutputType endpoint
  type OutputContentType endpoint

  endpoint :: Proxy endpoint -> Text

-- | Intermediate data to store inside our @freer-simple@ monad, so that
--   we can later interpret our request however we want.
data APIRequestData =
  APIRequestData
    { apiRequestBody :: ByteString
    , apiRequestContentType :: Text
    , apiRequestEndpoint :: Text
    }
  deriving (Eq, Show)

-- | Represents (part of) a computation that needs to make requests to the Slack
--   Web API. Meant to be used with @freer-simple@ (or some other freer monad
--   implementation) to allow for fine-grained effect permissioning and intepretation.
--
--   Instances of this shouldn't be constructed directly, but instead through
--   functions like `webAPICall' which lift it into a surrounding `Eff' context.
data SlackAPI a where
  WebAPICall :: APIRequestData -> SlackAPI ByteString
  -- TODO: We probably don't want this to be forced to return a ByteString,
  --       either, in case the HTTP transport layer fails. What to do in that case...

-- | Given an endpoint and its input, lift an API call into an `Eff' context,
--   returning the corresponding output type.
--
--   Meant to be used with @-XTypeApplications@ enabled, like so:
--
-- > response <- webAPICall @Chat_PostMessage $ Message
-- >   { text = "test message"
-- >   , channel = ...
-- >   , asUser = True
-- >   }
webAPICall :: forall endpoint inty incon outty outcon effs.
              -- make sure that `endpoint' is always first in the forall
              ( WebAPIEndpoint endpoint
              , inty ~ InputType endpoint
              , incon ~ InputContentType endpoint
              , outty ~ OutputType endpoint
              , outcon ~ OutputContentType endpoint
              , ContentType incon
              , OutputVerify outcon
              , EncodeConstraint incon inty
              , DecodeConstraint outcon outty
              , Member SlackAPI effs
              )
           => inty
           -> Eff effs (Either Text outty)
webAPICall input = do
  let reqData = toRequestData (Proxy @endpoint) input
  response <- send $ WebAPICall reqData
  pure $ eitherDecodeContent (Proxy @outcon) response

-- | Given an endpoint and its input, lift an API call into an `Eff' context,
--   ignoring the response.
--
--   Because we ignore the output, we actually don't need to ensure that the
--   output type satisfies the output constraint.
--
--   Meant to be used with @-XTypeApplications@ enabled, like so:
--
-- > webAPICall_ @Chat_PostMessage $ Message
-- >   { text = "test message"
-- >   , channel = ...
-- >   , asUser = True
-- >   }
webAPICall_ :: forall endpoint inty incon effs.
               -- make sure that `endpoint' is always first in the forall
               ( WebAPIEndpoint endpoint
               , inty ~ InputType endpoint
               , incon ~ InputContentType endpoint
               , ContentType incon
               , EncodeConstraint incon inty
               , Member SlackAPI effs
               )
            => inty
            -> Eff effs ()
webAPICall_ input = do
  let reqData = toRequestData (Proxy @endpoint) input
  _ <- send $ WebAPICall reqData
  pure ()

-- | Interpret the API calls directly, by sending them to the actual Slack API.
--   Slack requires that all requests be signed with an access token, so we
--   pass that in here as well. An access token usually starts with either
--   @xoxp@ or @xoxb@.
runSlackAPI :: (MonadIO m, LastMember m effs)
            => Text
            -> Eff (SlackAPI ': effs)
            ~> Eff effs
runSlackAPI accessToken eff = do
  manager <- liftIO $ newManager tlsManagerSettings
  (interpretM $ \case
    WebAPICall reqData -> runWebAPIRequest manager accessToken reqData) eff

-- | Interpret the API calls directly, by sending them to the actual Slack API.
--   Pull the Slack access token from the environment variable @SLACK_API_ACCESS_TOKEN@.
runSlackAPIEnv :: (MonadIO m, LastMember m effs)
               => Eff (SlackAPI ': effs)
               ~> Eff effs
runSlackAPIEnv eff = do
  token <- fmap pack $ liftIO $ getEnv "SLACK_API_ACCESS_TOKEN"
  runSlackAPI token eff

-- TODO: maybe we'll provide an in-memory interpreter for SlackAPI...

-- | Directly run an `APIRequestData' as an HTTP request and return the response
--   bytestring. The `Manager' must support HTTPS requests.
runWebAPIRequest :: MonadIO m => Manager -> Text -> APIRequestData -> m ByteString
runWebAPIRequest manager accessToken reqData = liftIO $ do
  response <- httpLbs request manager
  pure $ responseBody response
  where -- TODO: We want to check if Slack is returning signatures in responses.
        request :: Request
        request = defaultRequest
          { method = "POST"
          , secure = True
          , port = 443
          , host = "slack.com"
          , path = [i|/api/#{apiRequestEndpoint reqData}|]
          , requestBody = RequestBodyLBS $ apiRequestBody reqData
          , requestHeaders =
            [ ("Authorization", [i|Bearer #{accessToken}|])
            , ("Content-Length", [i|#{BL.length $ apiRequestBody reqData}|])
            , ("Content-Type", [i|#{apiRequestContentType reqData}|])
            ]
          }

-- | For a given endpoint, turn its corresponding input into a `ByteString'.
encodeEndpointInput :: forall endpoint inty incon.
                       ( WebAPIEndpoint endpoint
                       , inty ~ InputType endpoint
                       , incon ~ InputContentType endpoint
                       , ContentType incon
                       , EncodeConstraint incon inty
                       )
                    => Proxy endpoint
                    -> inty
                    -> ByteString
encodeEndpointInput _ = encodeContent (Proxy @incon)

-- | For a given endpoint, attempt to decode a `ByteString' as its output type.
decodeEndpointOutput :: forall endpoint outty outcon.
                        ( WebAPIEndpoint endpoint
                        , outty ~ OutputType endpoint
                        , outcon ~ OutputContentType endpoint
                        , ContentType outcon
                        , DecodeConstraint outcon outty
                        )
                     => Proxy endpoint
                     -> ByteString
                     -> Maybe outty
decodeEndpointOutput _ = decodeContent (Proxy @outcon)

toRequestData :: forall endpoint inty incon.
                 ( WebAPIEndpoint endpoint
                 , inty ~ InputType endpoint
                 , incon ~ InputContentType endpoint
                 , ContentType incon
                 , EncodeConstraint incon inty
                 )
              => Proxy endpoint
              -> inty
              -> APIRequestData
toRequestData _ input =
  APIRequestData
    { apiRequestBody = encodeEndpointInput (Proxy @endpoint) input
    , apiRequestContentType = contentSlug (Proxy @incon)
    , apiRequestEndpoint = endpoint (Proxy @endpoint)
    }
