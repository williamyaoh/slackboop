-- |
-- Module      : Chat.Slackboop.WebAPI.Raw
-- Description : Low-level interface to Slack's Web API
-- Copyright   : (c) William Yao, 2019
-- License     : BSD-3
-- Maintainer  : williamyaoh@gmail.com
-- Stability   : experimental
-- Portability : POSIX
--
-- This module defines the very lowest level primitives that Slackboop provides
-- for interacting with Slack's Web API; we use a minimum of advanced type features
-- and type guarantees to make extending this to work with your choice of
-- (monad stack|system architecture|HTTP client) as easy as possible.

{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE GADTSyntax        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE DeriveFunctor     #-}

module Chat.Slackboop.WebAPI.Raw
  (
  -- * Sending requests
  -- $sending-requests
    AuthToken(..), SlackRequestID(..)
  , sendWebAPIRequest, makeWebAPIRequest
  -- * Internals
  -- $internals
  , encodeHTTPRequest, authorizeRequest
  , extractResponse
  -- * Types
  , WebAPIEndpointPayload(..), WebAPIRequest(..), WebAPIResponse(..)
  , WebAPIError(..), WebAPIWarning(..)
  )
where

import qualified Prelude
import           Prologue

import           Data.Aeson              ( Value, eitherDecode, encode, toJSON )
import           Data.ByteString.Lazy    as LBS
import           Data.JSON.Typed
import qualified Data.List               as List
import           Data.Maybe              ( maybe, maybeToList )
import           Data.String             ( IsString )
import           Data.String.Interpolate ( i )
import           Web.FormUrlEncoded      ( Form, urlEncodeForm )

import Network.HTTP.Client     as HTTP
import Network.HTTP.Client.TLS

-- $errors
--
-- For every request, Slackboop checks various error conditions and reports
-- them in the response.
--
-- HTTP failure (4XX, 5XX) -> Authentication failure -> Slack-specific error
-- Slack /never/ specifies non-network errors using HTTP return codes.

data WebAPIEndpointPayload a = EndpointPayload
  { payload  :: a
  , endpoint :: Text
  }
  deriving (Eq, Show, Functor)

data WebAPIRequest where
  Get :: WebAPIEndpointPayload Form -> WebAPIRequest
  PostForm :: WebAPIEndpointPayload Form -> WebAPIRequest
  PostJSON :: WebAPIEndpointPayload Value -> WebAPIRequest
  deriving (Eq, Show)

data WebAPIResponse a = WebAPIResponse
  { slackRequestID      :: SlackRequestID  -- ^ A unique identifier that Slack passes us back.
  , slackWarnings       :: [WebAPIWarning]
  , slackResponseResult :: Either WebAPIError a
  }
  deriving (Eq, Show, Functor)

-- |
-- For now, we'll just return the machine-readable error code. Later, we can
-- make this more robust and give a way to actually pull out the corresponding
-- message.
newtype WebAPIError = WebAPIError { unWebAPIError :: Text }
  deriving (Eq, Show, IsString)

-- | Ditto.
newtype WebAPIWarning = WebAPIWarning { unWebAPIWarning :: Text }
  deriving (Eq, Show, IsString)

newtype AuthToken = AuthToken { unAuthToken :: Text }
  deriving (Eq, Show, IsString)

newtype SlackRequestID = SlackRequestID { unSlackRequestID :: Text }
  deriving (Eq, Show, IsString)

-- $sending-requests
--
-- Slackboop provides a default way of sending HTTP requests using @http-client@.
-- If you want to use a different HTTP client library, use the pure request
-- structures and hook them up to your client library of choice.
--
-- > -- :seti -XOverloadedStrings -XOverloadedLists
-- >
-- > let request = PostForm $ EndpointPayload
-- >       { endpoint = "chat.postMessage"
-- >       , payload = [("channel", "CA26NH7D0"), ("text", "test message")]
-- >       }
-- > makeWebAPIRequest "<AUTH TOKEN>" request
--
-- Use `sendWebAPIRequest' if you already have a `HTTP.Manager' to pass in.

type ResponseJSON = J '[ "ok" .! Bool, "error" .? Text, "warning" .? Text, ExtraProps ]

-- |
-- The beating heart of this module. Send the request to Slack and parse the
-- response. The provided `HTTP.Manager' __must__ support TLS.
sendWebAPIRequest :: HTTP.Manager -> AuthToken -> WebAPIRequest -> IO (WebAPIResponse Value)
sendWebAPIRequest manager token apiRequest = do
  let request = authorizeRequest token $ encodeHTTPRequest apiRequest

  response <- httpLbs request manager
  requestID <- case List.lookup "X-Slack-Req-ID" $ responseHeaders response of
                Nothing -> Prelude.fail "response from Slack is missing a request ID!"
                Just id -> pure id
  -- We can assume that this is an HTTP 200; Slack never uses HTTP codes to
  -- signify API errors.
  case extractResponse (SlackRequestID [i|#{requestID}|]) $ responseBody response of
    Left error     -> Prelude.fail [i|response from Slack is not valid JSON: #{error}|]
    Right response -> pure response

-- | Convenience wrapper around `sendWebAPIRequest' that constructs a connection manager.
makeWebAPIRequest :: AuthToken -> WebAPIRequest -> IO (WebAPIResponse Value)
makeWebAPIRequest token request = do
  manager <- newTlsManager
  sendWebAPIRequest manager token request

-- $internals
--
-- For convenience, Slackboop also provides these internal functions for
-- actually building requests to send to Slack's API. You'll most likely want
-- to use these if you're writing to target a different HTTP client.

-- |
-- We assume that the endpoint specified in the WebAPIRequest is valid for a
-- URL. If it isn't well... you'll find out when you run the request, I suppose.
encodeHTTPRequest :: WebAPIRequest -> HTTP.Request
encodeHTTPRequest request = case request of
  Get get -> baseRequest
    { path = [i|/api/#{endpoint get}|]
    , method = "GET"
    , queryString = [i|?#{urlEncodeForm $ payload get}|]
    , requestHeaders = [ ("Accept", "application/json;charset=utf-8") ]
    }
  PostForm post -> let bytes = urlEncodeForm $ payload post in baseRequest
    { path = [i|/api/#{endpoint post}|]
    , method = "POST"
    , requestBody = RequestBodyLBS bytes
    , requestHeaders =
      [ ("Content-Type", "application/x-www-form-urlencoded")
      , ("Content-Length", [i|#{LBS.length bytes}|])
      , ("Accept", "application/json;charset=utf-8")
      ]
    }
  PostJSON post -> let bytes = encode $ payload post in baseRequest
    { path = [i|/api/#{endpoint post}|]
    , method = "POST"
    , requestBody = RequestBodyLBS bytes
    , requestHeaders =
      [ ("Content-Type", "application/json;charset=utf-8")
      , ("Content-Length", [i|#{LBS.length bytes}|])
      , ("Accept", "application/json;charset=utf-8")
      ]
    }
  where baseRequest :: HTTP.Request
        baseRequest = defaultRequest
          { host = "www.slack.com"
          , port = 443
          , secure = True
          }

-- |
-- Add enough information for Slack to allow a Web API request
authorizeRequest :: AuthToken -> HTTP.Request -> HTTP.Request
authorizeRequest token request = request
  { requestHeaders =
    ("Authorization", [i|Bearer #{unAuthToken token}|]) : requestHeaders request
  }

-- |
-- Parse the HTTP body of a Slack response.
-- @requestID@ needs to be pulled from the @X-Slack-Req-ID@ header in Slack's response.
extractResponse :: SlackRequestID -> LBS.ByteString -> Either Prelude.String (WebAPIResponse Value)
extractResponse requestID body = case eitherDecode @ResponseJSON $ body of
    Left error -> Left error
    Right json -> do
      pure $ WebAPIResponse
        { slackRequestID = requestID
        , slackWarnings = fmap WebAPIWarning $ maybeToList $ getopt @"warning" json
        , slackResponseResult = result
        }
      where result :: Either WebAPIError Value
            result = if not $ getreq @"ok" json
              then Left $ WebAPIError (maybe "generic_error" id $ getopt @"error" json)
              else Right $ toJSON json
