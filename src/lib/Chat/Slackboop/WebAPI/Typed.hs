-- |
-- Module      : Chat.Slackboop.WebAPI.Typed
-- Description : Type-safe interface to Slack's Web API
-- Copyright   : (c) William Yao, 2019
-- License     : BSD-3
-- Maintainer  : williamyaoh@gmail.com
-- Stability   : experimental
-- Portability : POSIX
--
-- Here we slap some useful types on top of "Chat.Slackboop.WebAPI.Raw"'s low-level
-- primitives to make it easier to use.

{-# LANGUAGE AllowAmbiguousTypes  #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE InstanceSigs         #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeApplications     #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module Chat.Slackboop.WebAPI.Typed
  (
  -- * How it works
  -- $tying-a-knot
    Endpoint(..), webAPICall, makeWebAPICall
  , module Data.JSON.Typed
  -- * Slack API methods
  -- $specific-endpoints
  , Auth_Test
  -- * Internals
  , parseResponse
  )
where

import Prelude ()
import Prologue

import Data.Text       ( Text )
import Data.Aeson      ( FromJSON, Result(..), Value(Null), fromJSON )
import Data.JSON.Typed

import Network.HTTP.Client     as HTTP
import Network.HTTP.Client.TLS as HTTP

import GHC.Exts ( Constraint )

import Chat.Slackboop.Types
import Chat.Slackboop.WebAPI.Raw

-- $tying-a-knot
--
-- The heart of adding types to the API is the `Endpoint' typeclass, which
-- takes a specific Slack endpoint, such as @chat.postMessage@, and associates
-- with it what the format of its input and output are. Slackboop can then
-- automatically spit out correct HTTP requests, and users of the library
-- get their type safety.
--
-- > :seti -XTypeApplications -XOverloadedStrings
-- >
-- > manager <- newTlsManager
-- > webAPICall @Chat_PostMessage manager "<auth token>" $ jempty
-- >   |~ jreq @"channel" "CA26NH7D0"
-- >   |~ jreq @"text" "test message"
-- >   |~ jreq @"as_user" True

class Endpoint edpt where
  type InputConstraint edpt a :: Constraint
  type OutputFormat edpt :: *

  toRequest :: InputConstraint edpt a => a -> WebAPIRequest

parseResponse :: forall edpt. FromJSON (OutputFormat edpt)
              => WebAPIResponse Value
              -> WebAPIResponse (OutputFormat edpt)
parseResponse response =
  case sequenceA $ fromJSON @(OutputFormat edpt) <$> slackResponseResult response of
    Error _        -> response { slackResponseResult = Left $ WebAPIError "x_parse_error" }
    Success output -> response { slackResponseResult = output }

-- |
-- A convenience function for sending a request for a given `Endpoint'.
-- Uses @http-client@ internally to send HTTP requests.
webAPICall :: forall edpt a io. (Endpoint edpt, InputConstraint edpt a, FromJSON (OutputFormat edpt), MonadIO io)
           => HTTP.Manager
           -> AuthToken
           -> a
           -> io (WebAPIResponse (OutputFormat edpt))
webAPICall manager token dat =
  let request = toRequest @edpt dat
  in parseResponse @edpt <$> liftIO (sendWebAPIRequest manager token request)

-- |
-- A convenience for the convenience function that automatically constructs a
-- `HTTP.Manager'.
makeWebAPICall :: forall edpt a io. (Endpoint edpt, InputConstraint edpt a, FromJSON (OutputFormat edpt), MonadIO io)
               => AuthToken
               -> a
               -> io (WebAPIResponse (OutputFormat edpt))
makeWebAPICall token dat =
  liftIO HTTP.newTlsManager >>= \mgr -> webAPICall @edpt mgr token dat

-- $specific-endpoints
--
-- Instances for all of the Web API methods that Slack exposes.

data Auth_Test

instance Endpoint Auth_Test where
  type InputConstraint Auth_Test a = () ~ a
  type OutputFormat Auth_Test = J
    '[ "url" .! Text
     , "team" .! TeamName
     , "user" .! UserName
     , "team_id" .! TeamID
     , "user_id" .! UserID
     ]

  toRequest _ = PostJSON $ EndpointPayload { payload = Null, endpoint = "auth.test" }
