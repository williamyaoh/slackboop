# echo-bot

A simple example bot which simply copies each message you send to it
back to the channel that messaage was posted in.

## Usage

Booting this up is simple enough; just `stack exec echo-bot` after building.
You'll also probably want some way to provide an HTTPS tunnel for Slack to
send events to; the easiest way to do that is to install `ngrok` and run
`ngrok http 8080`. Finally, you'll need to set up your Slack application.

Create a new Slack application, install it to your workspace of choice, and
make sure that it has a bot user, as well as permission scope for
`chat:write:user`.

Once you have your bot set up correctly, set the environment variables
`SLACK_APP_ID`, `SLACK_SIGNING_SECRET`, and `SLACK_API_ACCESS_TOKEN` from
your app's information/OAuth permissions. Run `echo-bot` and point the event
subscription URL to your bot.
