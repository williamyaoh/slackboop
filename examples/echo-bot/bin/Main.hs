{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS -Wno-implicit-prelude #-}

module Main where

import Data.Maybe ( isJust )
import Data.Aeson
import Data.Text ( Text )

import Formatting
import Formatting.ShortFormatters

import System.IO

import Control.Monad.Freer.Trace

import Chat.Slackboop
import Chat.Slackboop.API
import qualified Chat.Slackboop.API.ChatPostMessage as C

data Message =
  Message
    { msgChannel :: Text
    , msgUser :: Text
    , msgTimestamp :: Text
    , msgText :: Text
    , msgIsBot :: Bool
    }
  deriving (Eq, Show)

instance FromJSON Message where
  parseJSON = withObject "Message" $ \obj -> do
    typeToken :: Text <- obj .: "type"
    if | typeToken /= "message" ->
         fail "didn't get `message' type"
       | otherwise -> do
         botID :: Maybe Text <- obj .:? "bot_id"
         Message
           <$> obj .: "channel"
           <*> obj .: "user"
           <*> obj .: "ts"
           <*> obj .: "text"
           <*> pure (isJust botID)

when :: Monad m => Bool -> m () -> m ()
when cond body =
  if cond then body else pure ()

msgHandler :: EventHandler
msgHandler = eventHandler $ \msg -> do
  let Message { .. } = msg
      output = formatToString ("user " % st % " sent message '" % st % "'") msgUser msgText
  when (not msgIsBot) $ do
    _ <- webAPICall @C.Chat_PostMessage $ C.Message
      { C.text = msgText
      , C.channel = msgChannel
      , C.asUser = True
      }
    trace output

main :: IO ()
main = do
  hPutStrLn stderr "booting up example Slack app on localhost:8080..."
  hPutStrLn stderr ""
  hPutStrLn stderr "  make sure to point Slack at localhost:8080/event, localhost:8080/interactive,"
  hPutStrLn stderr "  or localhost:8080/slashcmd"
  hPutStrLn stderr ""
  runSlackboopLocal 8080 slackboop
  where slackboop :: Slackboop
        slackboop =
          Slackboop
            { eventHandlers = [msgHandler]
            , interactionHandlers = []
            , slashCmdHandlers = []
            }
