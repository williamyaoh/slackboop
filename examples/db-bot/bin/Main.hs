{-# OPTIONS -Wno-implicit-prelude #-}
{-# OPTIONS -Wno-missing-monadfail-instances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}

module Main where

import Data.Text ( Text )
import qualified Data.Text as T
import Data.Text.IO ( hPutStrLn )
import Data.ByteString.Char8 ( pack )
import Data.Aeson
import Data.Maybe ( isJust )

import Database.PostgreSQL.Simple
import Control.Monad
import Control.Monad.Freer
import Control.Monad.IO.Class

import System.IO ( stderr )
import System.Environment ( getEnv )

import Text.Regex.TDFA
import Text.Regex.TDFA.Text ()

import Formatting hiding ( string )
import Formatting.ShortFormatters

import Chat.Slackboop

data Message =
  Message
    { msgChannel :: Text
    , msgUser :: Text
    , msgTimestamp :: Text
    , msgText :: Text
    , msgIsBot :: Bool
    }
  deriving (Eq, Show)

instance FromJSON Message where
  parseJSON = withObject "Message" $ \obj -> do
    typeToken :: Text <- obj .: "type"
    if | typeToken /= "message" ->
         fail "didn't get `message' type"
       | otherwise -> do
         botID :: Maybe Text <- obj .:? "bot_id"
         Message
           <$> obj .: "channel"
           <*> obj .: "user"
           <*> obj .: "ts"
           <*> obj .: "text"
           <*> pure (isJust botID)

data Review =
  Review
    { reviewScore :: Int
    , reviewText :: Text
    }
  deriving (Eq, Show)

data ReviewDB a where
  AddReview :: Review -> ReviewDB ()
  GetReviewAvg :: ReviewDB Double

addReview :: Member ReviewDB effs => Review -> Eff effs ()
addReview = send . AddReview

getReviewAvg :: Member ReviewDB effs => Eff effs Double
getReviewAvg = send GetReviewAvg

-- | Send inserts and queries to a remote PostgreSQL database. Assumes that
--   there is already a table called `reviews' in the database with a `score'
--   and `text' column.
runReviewDBPostgres :: (MonadIO m, LastMember m effs)
                    => Connection
                    -> Eff (ReviewDB ': effs)
                    ~> Eff effs
runReviewDBPostgres conn = interpretM $ \case
  AddReview review -> liftIO $ void $ execute conn
    "INSERT INTO reviews (score, text) VALUES (?,?)"
    (reviewScore review, reviewText review)
  GetReviewAvg -> do
    [Only h] <- liftIO $ query_ conn "SELECT AVG(CAST(score as DOUBLE PRECISION)) FROM reviews"
    pure h

type App = Eff '[ReviewDB, IO]

findScore :: Text -> Maybe Int
findScore t =
  let matches :: [Text] = getAllTextMatches (T.toLower t =~ stars)
  in case matches of
       [] -> Nothing
       (h:_) -> Just $ read $ T.unpack (h =~ digitT)

  where stars :: Text
        stars = "\\b[[:digit:]] stars?\\b"

        digitT :: Text
        digitT = "[[:digit:]]"

handleReview :: Review -> App ()
handleReview review = do
  addReview review
  avg <- getReviewAvg
  let output = sformat ("current avg of all reviews: " % sf) avg
  liftIO $ hPutStrLn stderr output

msgHandler :: EventHandler' App
msgHandler = eventHandler $ \msg -> do
  let Message { .. } = msg
  when (not msgIsBot) $ do
    case findScore msgText of
      Nothing -> pure ()
      Just score -> do
        let review = Review { reviewScore = score, reviewText = msgText }
        let output = sformat ("found review " % s % ", writing to database...") $ show review
        liftIO $ hPutStrLn stderr output
        handleReview review

main :: IO ()
main = do
  hPutStrLn stderr "booting up example Slack app on localhost:8080..."
  hPutStrLn stderr ""
  hPutStrLn stderr "  make sure to point Slack at localhost:8080/event, localhost:8080/interactive,"
  hPutStrLn stderr "  or localhost:8080/slashcmd"
  hPutStrLn stderr ""

  settings <- envSettings
  connString <- pack <$> getEnv "DB_BOT_POSTGRES_CONNECTION_STRING"
  conn <- connectPostgreSQL connString
  hoistSlackboopLocal (transform conn) 8080 slackboop settings

  where slackboop :: Slackboop' App
        slackboop =
          Slackboop
            { eventHandlers = [msgHandler]
            , interactionHandlers = []
            , slashCmdHandlers = []
            }

        transform :: Connection -> App ~> IO
        transform conn = runM . runReviewDBPostgres conn
