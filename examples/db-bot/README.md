# db-bot

A simple example bot with looks for messages with a certain format, and
extracts some data from them and writes them to a database.

## Usage

The bot assumes that you're writing to a PostgreSQL database, and pulls the
database connection string from the environment variable `DB_BOT_POSTGRES_CONNECTION_STRING`.
