# TODO

The work never ends... things that still need to be done:

## Major

* [x] Switch event handler(s) to use Slack signing token rather than
      verification token
* [x] Add verification or signing token authentication to interaction
      handlers
* [x] Add verification or signing token authentication to slash command
      handlers
* [x] Implement slash command handlers... like, at all.
* [x] Automatically use message return value on interaction handlers to
      send a response to Slack [TABLED]
* [ ] Rewire AWS Lambda backend to correctly handle incoming interactions
      and slash commands
* [x] Add some sort of logging Eff monad into our default stack, so that
      we're not constantly in the dark as to what's happening...

## Minor

* [ ] Create version of AWS Lambda backend that opens a socket to subprocess
      instead of forking on each request
* [ ] Have local server backend send each request to a worker thread and
      return 200 OK from the handling thread immediately
* [ ] Have AWS Lambda backend send incoming requests to a Kinesis stream
      (or SQS?) and provide a way to build a second, separate Lambda function
      for handling them; return 200 OK from the main Lambda immediately
* [ ] Hook callbacks from interaction flows into the type level and make
      get compile-time guarantees in our interaction handlers

## Polish

* [ ] Log when requests come into the Slack app with incorrect authentication
      information as a warning
* [ ] Log when requests come into the Slack app with a different app ID as
      an info
* [ ] Allow different monads for event and interaction handling flows
* [ ] Rename all non-typename usages of "interaction" to "itxn", add notes about
      jargon in module description of Slack
