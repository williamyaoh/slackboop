# `bot-message` vs `bot-user-message`

There's two ways an app can send a message to a channel: either through a bot
user, or as the app itself, based on whether `as_user' is set to true or
false when you call `chat.postMessage' and friends. Which way you choose
will produce different-looking messages; `bot-message` is what it looks like
when a bot sends a message directly through its app; `bot-user-message` is
what it looks like when a message is sent through the bot user.
