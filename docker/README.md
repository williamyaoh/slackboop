# slackboop/docker

Build specification for a Docker image containing most of Slackboop's
transitive dependencies precompiled. Mostly used for CI, to speed
up builds.

Run `sudo docker build .` in this directory to build an image.
