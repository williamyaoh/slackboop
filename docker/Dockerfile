FROM nixos/nix:2.0.4

# Annoyingly, we have to manually add in Certificate Authority certificates
# to our image so that we can do... anything online.

ADD ./digicert.crt digicert.crt
ADD ./config.yaml /root/.stack/config.yaml
ADD ./stack.yaml /root/.stack/global-project/stack.yaml
RUN apk update
RUN apk add ca-certificates
RUN update-ca-certificates digicert.crt

RUN nix-channel --add https://nixos.org/channels/nixos-18.03 nixpkgs
RUN nix-channel --update
RUN nix-env -f '<nixpkgs>' -iA stack haskell.compiler.ghc822

RUN stack build --system-ghc --fast \
  safe data-default unordered-containers justified-containers \
  containers text bytestring raw-strings-qq these formatting \
  aeson lens monoid-extras monad-loops bifunctors recursion-schemes \
  freer-simple
RUN stack build --system-ghc --fast \
  servant servant-server warp http-client http-client-tls http-api-data \
  cryptonite time